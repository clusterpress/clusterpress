<?php
/**
 * ClusterPress Single User Manage Account template.
 *
 * @package ClusterPress\templates\user\single\manage
 * @subpackage account
 *
 * @since 1.0.0
 */
?>

<form id="<?php cp_user_account_form_id(); ?>" class="edit-user-account" method="post" action="">

	<input type="text" id="cp_field_username" name="cp_account_edit[username]" value="<?php cp_user_account_username(); ?>" <?php cp_account_username_disabled(); ?>>
	<label class="label" for="cp_field_username">
		<?php cp_user_account_username_label(); ?>
	</label>
	<p class="description"><?php cp_user_account_username_description(); ?></p>

	<select name="cp_account_edit[display_name]" id="cp_field_display_name">

		<?php cp_user_account_display_name_options(); ?>

	</select>
	<label class="label" for="cp_field_display_name">
		<?php cp_user_account_display_name_label(); ?>
	</label>

	<input type="text" id="cp_field_email" name="email" value="<?php cp_user_account_email(); ?>">
	<label class="label" for="cp_field_email">
		<?php cp_user_account_email_label(); ?>
	</label>

	<?php if ( cp_user_account_new_email_pending() ) : ?>

		<p class="description"><?php cp_user_account_dismiss_email_change(); ?></p>

	<?php endif; ?>

	<?php if ( cp_user_show_avatar() ) : ?>

		<div class="cp-edit-avatar">

			<?php cp_user_avatar( array( 'width' => 40, 'height' => 40 ) ); ?>

		</div>
		<label class="label">
			<?php cp_user_account_avatar_label(); ?>
		</label>
		<p class="description"><?php cp_user_account_avatar_description(); ?></p>

	<?php endif ; ?>

	<?php cp_user_account_edit_password(); ?>

	<?php cp_account_submit_button( 'cp_account_edit' ); ?>

</form>
