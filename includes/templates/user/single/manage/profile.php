<?php
/**
 * ClusterPress Single User Manage Profile template.
 *
 * @package ClusterPress\templates\user\single\manage
 * @subpackage profile
 *
 * @since 1.0.0
 */

if ( cp_user_has_fields() ) : ?>

	<form class="edit-user-profile" method="post" action="">

		<?php while ( cp_user_the_fields() ) : cp_user_the_field() ; ?>

			<?php if ( cp_user_is_description() ) : ?>

				<textarea id="cp_field_<?php cp_user_field_id(); ?>" name="cp_profile_edit[<?php cp_user_field_id(); ?>]"><?php echo esc_textarea( cp_user_get_field_value( 'edit' ) ); ?></textarea>

			<?php else : ?>

				<input type="text" id="cp_field_<?php cp_user_field_id(); ?>" name="cp_profile_edit[<?php cp_user_field_id(); ?>]" value="<?php echo esc_attr( cp_user_get_field_value( 'edit' ) ); ?>">

			<?php endif ; ?>

			<label class="label" for="cp_field_<?php cp_user_field_id(); ?>">

				<?php cp_user_field_name(); ?>

			</label>

		<?php endwhile ; ?>

		<?php cp_submit_button( 'cp_profile_edit' ); ?>

	</form>

<?php else :

	cp_user_no_profile_found();

endif;
