<?php
/**
 * ClusterPress Single Site Manage Followers template.
 *
 * @package ClusterPress\templates\site\single\manage
 * @subpackage followers
 *
 * @since 1.0.0
 */

cp_site_start_followers_loop() ; ?>

<div id="cp-site-followers" class="users archive">

	<?php cp_get_template_part( 'user/loops/users-table' ) ; ?>

</div>

<?php cp_site_end_followers_loop() ;
