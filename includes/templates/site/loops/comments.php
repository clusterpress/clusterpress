<?php
/**
 * ClusterPress Site Comments loop template.
 *
 * @package ClusterPress\templates\site\loops
 * @subpackage comments
 *
 * @since 1.0.0
 */

if ( cp_site_has_comments() ) : ?>

	<?php if ( cp_site_comments_has_pagination() ) : ?>

		<div class="cp-pagination top">

			<div class="cp-total-count">

				<?php cp_site_comments_total_count(); ?>

			</div>

			<?php if ( cp_site_comments_has_pagination_links() ) : ?>

				<div class="cp-pagination-links">

					<?php cp_site_comments_pagination_links(); ?>

				</div>

			<?php endif ; ?>

		</div>

	<?php endif ; ?>

	<ul class="comment-list">

		<?php while ( cp_site_the_comments() ) : cp_site_the_comment() ; ?>

			<li class="<?php cp_site_the_comment_classes(); ?>">

				<p class="cp-comment-meta"><?php cp_site_the_comment_date(); ?></p>

				<?php if ( cp_site_the_comment_is_awaiting_moderation() ) : ?>

					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Votre commentaire est en attente de moderation.', 'clusterpress' ); ?></p>

				<?php endif ;?>

				<?php cp_site_the_comment_excerpt(); ?>

				<?php cp_site_the_comment_actions() ;?>

			</li>

		<?php endwhile ; ?>

	</ul><!-- // .comment-list -->

	<?php if ( cp_site_comments_has_pagination_links() ) : ?>

		<div class="cp-pagination bottom">

			<div class="cp-pagination-links">

				<?php cp_site_comments_pagination_links(); ?>

			</div>

		</div>

	<?php endif ; ?>

<?php else :

	cp_site_no_comments_found();

endif;
