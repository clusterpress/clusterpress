<?php
/**
 * Functions specific to the Site following feature.
 *
 * @package ClusterPress\site\multisite\following-sites
 * @subpackage functions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Capabilities callback for Site's following.
 *
 * @since 1.0.0
 *
 * @param int   $user_id The user ID
 * @param array $args    Adds the context to the cap. Typically the object ID.
 */
function cp_site_follow_required_caps( $user_id = 0, $args = array() ) {
	$caps = array( 'do_not_allow' );

	if ( ! cp_site_is_following_enabled() || empty( $user_id ) ) {
		return $caps;
	}

	if ( ! empty( $args[0]['site'] ) && is_a( $args[0]['site'], 'WP_Site' ) ) {
		$site = $args[0]['site'];
		$current_user = wp_get_current_user();

		if ( empty( $current_user->sites_level ) ) {
			$current_user->sites_level = array();
		}

		if ( empty( $current_user->sites_level[ $site->blog_id ] ) ) {
			$current_user->sites_level = $current_user->sites_level + array( $site->blog_id => cp_sites_get_user_level( $current_user->ID, $site->blog_id ) );
		}

		if ( ! empty( $current_user->sites_level[ $site->blog_id ]->user_level ) ) {
			return $caps;
		} else {
			$caps = array( 'exist' );
		}
	}

	/**
	 * Filter here to edit the site's following caps
	 *
	 * @since 1.0.0
	 *
	 * @param  array   $caps    The user's actual capabilities.
	 * @param  int     $user_id The user ID.
	 * @param  array   $args    Adds the context to the cap. Typically the object ID.
	 */
	return apply_filters( 'cp_site_follow_required_caps', $caps, $user_id, $args );
}

/**
 * Get the unread latest posts of a user's followed sites.
 *
 * @since 1.0.0
 *
 * @param int $user_id The user ID.
 */
function cp_sites_get_user_followed_sites_new_posts( $user_id = 0 ) {
	$new_posts = array();

	if ( ! cp_user_relationships_isset() || empty( $user_id ) ) {
		return $new_posts;
	}

	$new_posts = wp_cache_get( $user_id, 'cp_site_followed_new_posts' );

	if ( false === $new_posts ) {
		$new_posts = clusterpress()->user->relationships->get( array(
			'id',
			'primary_id',
			'secondary_id',
			'name',
			'date',
		), array(
			'user_id' => $user_id,
			'name'    => array( 'followed_site_new_post' )
		) );

		// Get user sites
		$user_followed_sites = cp_get_sites( array(
			'user_id'     => $user_id,
			'user_level'  => 0,
			'fields'      => 'ids',
			'count_total' => false,
		) );

		// Makes sure the user is still following the site.
		foreach ( $new_posts as $knp => $vnp ) {
			if ( false !== array_search( $vnp->secondary_id, $user_followed_sites ) ) {
				continue;
			}

			unset( $new_posts[ $knp ] );
		}

		wp_cache_set( $user_id, $new_posts, 'cp_site_followed_new_posts' );
	}

	return $new_posts;
}

/**
 * Edit the Toolbar Followed Sites node to append the unread latest posts count.
 *
 * @since 1.0.0
 *
 * @param  array   $nodes The User toolbar nav items.
 * @param  WP_User $user  The User object.
 * @return array          The User toolbar nav items.
 */
function cp_sites_get_user_followed_sites_new_posts_count( $nodes = array(), $user = null ) {
	if ( ! empty( $user->ID ) && (int) $user->ID === (int) get_current_user_id() ) {
		$id = cp_site_get_user_following_rewrite_id();

		if ( empty( $nodes[ $id ] ) ) {
			return $nodes;
		}

		$new_posts = cp_sites_get_user_followed_sites_new_posts( $user->ID );

		if ( $new_posts ) {
			$nodes[ $id ]->count = count( $new_posts );
		}
	}

	return $nodes;
}
add_filter( 'cp_user_cluster_get_toolbar_urls', 'cp_sites_get_user_followed_sites_new_posts_count', 15, 2 );

/**
 * Delete all followed content for a post or a site.
 *
 * @since  1.0.0
 *
 * @param  array  $args {
 *    @type int    $primary_id   The content ID (Post ID).
 *    @type int    $secondary_id The content parent ID (Site ID).
 *    @name string $name         The relationship name.
 * }
 * @return bool         True on success. False otherwise.
 */
function cp_sites_delete_site_followed_content( $args = array() ) {
	$r = wp_parse_args( $args, array(
		'primary_id'   => 0,
		'secondary_id' => (int) get_current_blog_id(),
		'name'         => 'followed_site_new_post',
	) );

	if ( empty( $r['name'] ) || ( empty( $r['primary_id'] ) && empty( $r['secondary_id'] ) ) ) {
		return false;
	}

	$site_followed_content = cp_user_get_relationship( array( 'id', 'user_id' ), array_filter( $r ) );

	/**
	 * Hook here if you need to perform custom actions before the followed unread content is removes.
	 *
	 * @since 1.0.0
	 *
	 * @param object $site_followed_content The relationship object.
	 */
	do_action( 'cp_sites_before_delete_site_followed_content', $site_followed_content );

	// Init deleted to false
	$deleted = false;

	if ( ! empty( $site_followed_content ) ) {
		$user_ids = wp_list_pluck( $site_followed_content, 'user_id', 'id' );
		$arg      = array( 'id' => array_keys( $user_ids ) );

		if ( ! empty( $arg['id'] ) ) {
			$user_ids = array_unique( $user_ids );

			if ( cp_user_remove_relationship( $arg ) ) {
				// It's a success!
				$deleted = true;

				// Clean each user's mention cache.
				foreach ( $user_ids as $user_id ) {
					/**
					 * Hook here if you need to perform actions for each user once the content is deleted.
					 *
					 * @since 1.0.0
					 *
					 * @param int $user_id The user ID.					 *
					 */
					do_action( 'cp_sites_delete_site_followed_content_for_user', $user_id );
				}
			}
		}
	}

	/**
	 * Hook here if you need to perform custom actions after the followed unread content is removes.
	 *
	 * @since 1.0.0
	 *
	 * @param bool $deleted True if unread followed content was removed. False otherwise.
	 */
	do_action( 'cp_sites_after_delete_site_followed_content', $deleted );

	return $deleted;
}

/**
 * Rest API permission to get Unread Followed content callback.
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return bool True if the user can get Unread posts. False otherwise.
 */
function cp_site_user_can_get_followed_site_new_posts( WP_REST_Request $request ) {
	$user_id = (int) $request->get_param( 'id' );

	return current_user_can( 'cp_edit_single_user', array( 'user_id' => $user_id ) );
}

/**
 * Handle Rest API GET Unread Followed content for a requested user ID
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return array                    An array containing the post title and the post content.
 */
function cp_site_get_user_followed_sites_new_posts( WP_REST_Request $request ) {
	global $post;

	$reset_post      = $post;
	$user_id         = (int) $request->get_param( 'id' );
	$relationship_id = (int) $request->get_param( 'relationship_id' );
	$error           = array( 'error' => __( 'L\'article n\'a pas été trouvé.', 'clusterpress' ) );

	if ( ! $relationship_id || ! cp_user_relationships_isset() ) {
		return $error;
	}

	// Validate the requested post
	$postdata = clusterpress()->user->relationships->get( array(
		'primary_id',
		'secondary_id',
	), array(
		'id' => $relationship_id,
	) );

	if ( ! $postdata ) {
		return $error;
	}

	$return   = $error;
	$postdata = reset( $postdata );

	switch_to_blog( $postdata->secondary_id );
	$post = get_post( $postdata->primary_id );
	setup_postdata( $post );

	$return = array(
		'post_title'   => get_the_title(),
		'post_content' => get_the_excerpt(),
	);

	wp_reset_postdata();
	restore_current_blog();
	$post = $reset_post;

	return $return;
}

/**
 * Rest API permission to follow/unfollow site.
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return bool True if the user can follow/unfollow site. False otherwise.
 */
function cp_site_user_can_follow_site( WP_REST_Request $request ) {
	$site_id = (int) $request->get_param( 'id' );
	$site    = cp_get_site_by( 'id', $site_id );

	return current_user_can( 'cp_can_follow_site', array( 'site' => $site ) );
}

/**
 * Handle Rest API POST to follow a site
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return array                    An array containing the relationship ID and the Button html.
 */
function cp_site_user_follow_site( WP_REST_Request $request ) {
	$site_id   = (int) $request->get_param( 'id' );
	$user_id   = get_current_user_id();
	$follow_id = cp_sites_add_user( $user_id, $site_id, 0, 'last_insert_id' );

	if ( ! $follow_id ) {
		return array( 'error' => __( 'Ce site ne peut pas être suivi par des adeptes.', 'clusterpress' ) );
	}

	return array(
		'follow_id' => $follow_id,
		'text'      => sprintf( __( 'Ne plus suivre<span class="screen-reader-text"> "%s"</span>', 'clusterpress' ),
			esc_html( cp_sites_get_meta( $site_id, 'name', true ) )
		),
	);
}

/**
 * Handle Rest API DELETE to unfollow a site
 *
 * @since  1.0.0
 *
 * @param  WP_REST_Request $request The Rest Request object.
 * @return array                    An array containing the relationship ID and the Button html.
 */
function cp_site_user_unfollow_site( WP_REST_Request $request ) {
	$site_id    = (int) $request->get_param( 'id' );
	$user_id    = get_current_user_id();
	$follow_id  = (int) $request->get_param( 'follow_id' );
	$unfollowed = cp_sites_remove_user_by_id( $follow_id );

	if ( ! $unfollowed ) {
		return array( 'error' => __( 'Une erreur est survenue, merci de renouveler l\'opération plus tard.', 'clusterpress' ) );
	}

	return array(
		'text' => sprintf( __( 'Suivre<span class="screen-reader-text"> "%s"</span>', 'clusterpress' ),
			esc_html( cp_sites_get_meta( $site_id, 'name', true ) )
		),
	);
}
