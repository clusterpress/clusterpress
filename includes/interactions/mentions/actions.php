<?php
/**
 * ClusterPress Mentions Actions.
 *
 * @package ClusterPress\interactions\mentions
 * @subpackage actions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Register the User Mentions section.
 *
 * @since 1.0.0
 */
function cp_interactions_register_mention_section() {

	cp_register_cluster_section( 'user', array(
		'id'            => 'mentions',
		'name'          => __( 'Mentions', 'clusterpress' ),
		'cluster_id'    => 'user',
		'slug'          => cp_user_get_mentions_slug(),
		'rewrite_id'    => cp_user_get_mentions_rewrite_id(),
		'template'      => '',
		'template_dir'  => '',
		'toolbar_items' => array(
			'position' => 50,
			'dashicon' => 'dashicons-format-chat',
			'count'    => 0,
		),
	) );
}
add_action( 'cp_register_cluster_sections', 'cp_interactions_register_mention_section' );

/**
 * Register the User mentions relationships.
 *
 * @since 1.0.0
 */
function cp_interactions_register_mention_relationships() {

	cp_user_register_relationships( 'user', array(
		// Mentions in posts
		array(
			'name'             => 'post_mentions',
			'cluster'          => 'user',
			'singular'         => __( 'l\'article', 'clusterpress'  ),
			'plural'           => __( 'les articles', 'clusterpress' ),
			'description'      => __( 'Mentions d\'article', 'clusterpress' ),
			'format_callback'  => array(
				'selfnew' => esc_html__( 'Vous avez été nouvellement mentionné dans un article.', 'clusterpress' ),
				'self'    => esc_html__( 'Vous avez été mentionné dans un article et en avez pris connaissance %s.', 'clusterpress' ),
				'read'    => esc_html__( '%1$s a été mentionné dans un article et en a pris connaissance %2$s.', 'clusterpress' ),
				'unread'  => esc_html__( '%s a été mentionné dans un article mais n\'en a pas pris connaissance jusqu\'à présent.', 'clusterpress' ),
			),
		),
		// Mentions in comments
		array(
			'name'             => 'comment_mentions',
			'cluster'          => 'user',
			'singular'         => __( 'le commentaire', 'clusterpress'  ),
			'plural'           => __( 'les commentaires', 'clusterpress' ),
			'description'      => __( 'Mentions de commentaire', 'clusterpress' ),
			'format_callback'  => array(
				'selfnew' => esc_html__( 'Vous avez été nouvellement mentionné dans un commentaire.', 'clusterpress' ),
				'self'    => esc_html__( 'Vous avez été mentionné dans un commentaire et en avez pris connaissance %s.', 'clusterpress' ),
				'read'    => esc_html__( '%1$s a été mentionné dans un commentaire et en a pris connaissance  %2$s.', 'clusterpress' ),
				'unread'  => esc_html__( '%s a été mentionné dans un commentaire mais n\'en a pas pris connaissance jusqu\'à présent.', 'clusterpress' ),
			),
		),
	) );
}
add_action( 'cp_register_relationships', 'cp_interactions_register_mention_relationships' );

/**
 * Add/edit/delete mentions once Posts and Comments content is published/updated/trashed.
 *
 * @since 1.0.0
 *
 * @param string             $new_status The updated status for the Post or the Comment.
 * @param string             $old_status The previous status of the Post or the Comment.
 * @param WP_Post|WP_Comment $object     The Post or Comment object.
 */
function cp_interactions_mention_users( $new_status = '', $old_status = '', $object = null ) {
	if ( empty( $object ) ) {
		return;
	}

	$existing_mentions = array();

	if ( is_a( $object, 'WP_Post' ) ) {
		$primary_id        = $object->ID;
		$status            = 'publish';
		$existing_mentions = (array) get_post_meta( $primary_id, '_clusterpress_post_mentions', true );
		$content           = $object->post_content;
		$post_type         = $object->post_type;
		$name              = $post_type . '_mentions';
		$feature           = 'clusterpress_post_mentions';

	} elseif ( is_a( $object, 'WP_Comment' ) ) {
		$primary_id        = $object->comment_ID;
		$status            = 'approved';
		$existing_mentions = (array) get_comment_meta( $primary_id, '_clusterpress_comment_mentions', true );
		$content           = $object->comment_content;
		$name              = 'comment_mentions';
		$post_type         = get_post_field( 'post_type', $object->comment_post_ID );
		$feature           = 'clusterpress_comment_mentions';

	} else {
		return;
	}

	if ( empty( $primary_id ) || ! post_type_supports( $post_type, $feature ) ) {
		return;
	}

	if ( ! cp_user_relationships_get_object( $name ) ) {
		return;
	}

	if ( $new_status !== $status ) {
		return cp_interactions_delete_mentions( array(
			'primary_id'   => (int) $primary_id,
			'name'         => $name,
		) );
	}

	// Avoid duplicates
	$user_nicenames = cp_interactions_parse_mentions( $content );

	// Bail if no usernames.
	if ( empty( $user_nicenames ) ) {
		return;
	}

	$existing_clone = $existing_mentions;
	$user_nicenames = array_diff( $user_nicenames, array_keys( $existing_mentions ) );

	if ( empty( $user_nicenames ) ) {
		return;
	}

	$secondary_id = (int) get_current_blog_id();
	$is_main_site = cp_is_main_site( $secondary_id );

	if ( ! $is_main_site ) {
		switch_to_blog( get_main_network_id() );
	}

	// We've found some mentions! Check to see if users exist.
	foreach ( $user_nicenames as $user_nicename ) {
		$user = get_user_by( 'slug', $user_nicename );

		// The user ID exists, so let's add it to our array.
		if ( ! empty( $user->ID ) ) {
			$existing_mentions[ $user_nicename ] = cp_user_get_url( array(), $user );

			cp_interactions_add_mention( $user, $primary_id, $secondary_id, $name );
		}
	}

	if ( ! $is_main_site ) {
		restore_current_blog();
	}

	if ( count( $existing_clone ) !== count( $existing_mentions ) ) {
		$mentions = array_filter( $existing_mentions );

		if ( 'approved' === $status ) {
			update_comment_meta( $primary_id, '_clusterpress_comment_mentions', $mentions );
		} else {
			update_post_meta( $primary_id, '_clusterpress_post_mentions', $mentions );
		}
	}
}
add_action( 'transition_post_status',    'cp_interactions_mention_users', 10, 3 );
add_action( 'transition_comment_status', 'cp_interactions_mention_users', 10, 3 );

/**
 * Add a missing transition comment status to WordPress.
 *
 * @since 1.0.0
 *
 * @param int        $comment_ID The ID of the comment just inserted.
 * @param WP_Comment $comment    The comment object just inserted.
 */
function cp_interactions_transition_comment_status( $comment_ID = 0, $comment = null ) {
	if ( empty( $comment_ID ) || empty( $comment->comment_approved ) || 1 !== (int) $comment->comment_approved ) {
		return;
	}

	return cp_interactions_mention_users( 'approved', '', $comment );
}
add_action( 'wp_insert_comment', 'cp_interactions_transition_comment_status', 10, 2 );

/**
 * Handle the user actions about his mentions
 *
 * @since 1.0.0
 */
function cp_interactions_handle_actions() {
	if ( ! cp_is_current_action( cp_user_get_mentions_slug() ) ) {
		return;
	}

	$user_id  = cp_get_displayed_user_id();

	if ( ! empty( $_GET['readall'] ) ) {

		check_admin_referer( 'cp_mention_bulk_read' );

		$redirect = remove_query_arg( array( 'readall', '_wpnonce' ), wp_get_referer() );

		if ( ! cp_user_is_self_profile() || ! cp_interactions_mark_all_mentions_read( $user_id ) ) {
			cp_feedback_redirect( 'mention-03', 'error', $redirect );
		}

		cp_feedback_redirect( 'mention-05', 'updated', $redirect );

	} elseif ( ! empty( $_GET['redirectto'] ) ) {
		$mention_id = (int) $_GET['redirectto'];

		check_admin_referer( 'cp_view_mention' );

		$mention = clusterpress()->user->relationships->get( array(
			'id',
			'user_id',
			'primary_id',
			'secondary_id',
			'name',
			'date',
		), array(
			'id' => $mention_id,
		) );

		$redirect = remove_query_arg( array( 'redirectto', '_wpnonce' ), wp_get_referer() );

		if ( empty( $mention ) ) {
			cp_feedback_redirect( 'mention-01', 'error', $redirect );
		} else {
			$mention = reset( $mention );
		}

		// Remove the mention from the unread by updating its date
		if ( empty( $mention->date ) && cp_user_is_self_profile() ) {
			$date = current_time( 'mysql', 1 );

			if ( ! cp_user_update_relationship( array( 'date' => $date ), array( 'id' => $mention_id, 'name' => $mention->name ) ) ) {
				cp_feedback_redirect( 'mention-02', 'error', $redirect );
			}

			do_action( 'cp_interactions_mention_read', $user_id, $mention );
		}

		$is_main_site = cp_is_main_site( $mention->secondary_id );

		if ( ! $is_main_site ) {
			switch_to_blog( $mention->secondary_id );
		}

		// Finally redirect users to the object the mention is attached to.
		if ( 'comment_mentions' === $mention->name ) {
			$redirect = get_comment_link( $mention->primary_id );
		} elseif ( false !== array_search( $mention->name, cp_interactions_get_post_type_mention_names() ) ) {
			$redirect = get_permalink( $mention->primary_id );
		}

		/**
		 * Filter here to edit the redirect link.
		 *
		 * @since 1.0.0
		 *
		 * @param  string $redirect The url to redirect the user to.
		 * @param  object $mention  The mention object.
		 */
		$redirect = apply_filters( 'cp_interactions_mention_redirect_customs', $redirect, $mention );

		if ( ! $is_main_site ) {
			restore_current_blog();
		}

		wp_redirect( $redirect );
		exit();
	}
}
add_action( 'cp_page_actions', 'cp_interactions_handle_actions' );

/**
 * Clean the User's new mentions cache when needed.
 *
 * @since 1.0.0
 *
 * @param int $user_id The current user ID.
 */
function cp_interactions_reset_mention_cache( $user_id = 0 ) {
	wp_cache_delete( $user_id, 'cp_user_mentions' );
}
add_action( 'cp_interactions_add_mention',             'cp_interactions_reset_mention_cache', 10, 1 );
add_action( 'cp_interactions_mention_read',            'cp_interactions_reset_mention_cache', 10, 1 );
add_action( 'cp_interactions_mention_all_read',        'cp_interactions_reset_mention_cache', 10, 1 );
add_action( 'cp_interactions_delete_mention_for_user', 'cp_interactions_reset_mention_cache', 10, 1 );

/**
 * Remove all mentions belonging to a just removed site.
 *
 * @since 1.0.0
 *
 * @param int $site_id The removed site ID.
 */
function cp_interactions_remove_mentions_for_site( $site_id = 0 ) {
	if ( empty( $site_id ) ) {
		return;
	}

	return cp_interactions_delete_mentions( array(
		'secondary_id' => $site_id,
		'name'         => array_merge( cp_interactions_get_post_type_mention_names(), array( 'comment_mentions' ) ),
	) );
}
add_action( 'delete_blog', 'cp_interactions_remove_mentions_for_site', 10, 1 );
