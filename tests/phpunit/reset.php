<?php
/**
 * Reset ClusterPress for the purpose of the unit-tests
 */
error_reporting( E_ALL & ~E_DEPRECATED & ~E_STRICT );

$config_file_path = $argv[1];
$tests_dir_path = $argv[2];
$multisite = ! empty( $argv[3] );

require_once $config_file_path;
require_once $tests_dir_path . '/includes/functions.php';

require_once ABSPATH . '/wp-settings.php';

echo "Resetting ClusterPress tables..." . PHP_EOL;

if ( version_compare( $wpdb->db_version(), '5.5.3', '>=' ) ) {
	$wpdb->query( 'SET default_storage_engine = InnoDB' );
} else {
	$wpdb->query( 'SET storage_engine = InnoDB' );
}
$wpdb->select( DB_NAME, $wpdb->dbh );

// Drop ClusterPress tables.
foreach ( $wpdb->get_col( "SHOW TABLES LIKE '" . $wpdb->prefix . "cp_%'" ) as $cp_table ) {
	$wpdb->query( "DROP TABLE {$cp_table}" );
}
