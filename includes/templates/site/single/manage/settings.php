<?php
/**
 * ClusterPress Single Site Manage Settings template.
 *
 * @package ClusterPress\templates\site\single\manage
 * @subpackage settings
 *
 * @since 1.0.0
 */
?>

<form class="edit-site-settings" method="post" action="">

	<fieldset id="cp-site-settings" class="settings">
		<legend><?php esc_html_e( 'Options du site', 'clusterpress' ); ?></legend>

		<?php cp_get_template_part( 'site/loops/settings' ) ; ?>

	</fieldset>

	<?php if ( cp_site_manage_has_categories() ) : ?>

		<fieldset id="cp-site-categories" class="categories">
			<legend><?php esc_html_e( 'Catégories assignées au site', 'clusterpress' ); ?></legend>

			<?php cp_site_manage_the_category_checklist(); ?>

		</fieldset>

	<?php endif ;?>

	<?php cp_submit_button( 'cp_site_settings' ); ?>

</form>
