<?php
/**
 * ClusterPress Upgrade.
 *
 * @package ClusterPress\core
 * @subpackage upgrade
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Does the plugin needs to be upgraded ?
 *
 * @since 1.0.0
 *
 * @return bool True if it's an upgrade. False otherwise.
 */
function cp_is_upgrade() {
	return version_compare( cp_get_raw_db_version(), cp_get_db_version(), '<' );
}

/**
 * Is this the first install of the plugin ?
 *
 * @since 1.0.0
 *
 * @return bool True if it's the first install. False otherwise.
 */
function cp_is_install() {
	return 0 === cp_get_raw_db_version();
}

/**
 * Run the upgrade routines.
 *
 * NB: The upgrade or install routines are all
 * located into the CP_Cluster class of each
 * Clusters.
 *
 * @since 1.0.0
 */
function cp_upgrade() {
	if ( ! cp_is_upgrade() && ! cp_is_install() ) {
		return;
	}

	$db_version = cp_get_db_version();

	// Include the WordPress Upgrader.
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	if ( cp_is_install() ) {
		/**
		 * Trigger the 'cp_install_clusters' action so
		 * that each cluster can run their install tasks.
		 *
		 * @since 1.0.0
		 */
		do_action( 'cp_install_clusters' );

	} elseif ( cp_is_upgrade() ) {
		/**
		 * Trigger the 'cp_upgrade_clusters' action so
		 * that each cluster can run their upgrade tasks.
		 *
		 * @since 1.0.0
		 */
		do_action( 'cp_upgrade_clusters', $db_version );
	}

	// Always refresh the rewrite rules.
	cp_delete_rewrite_rules();

	// Update the raw db version.
	update_network_option( 0, '_clusterpress_version', $db_version );
}
add_action( 'cp_admin_init', 'cp_upgrade', 999 );

/**
 * Redirect the user to the About ClusterPress on activation.
 *
 * @since  1.0.0
 */
function cp_about_clusterpress_redirect() {
	$redirect = get_site_transient( '_clusterpress_activation_redirect' );

	// Bail if no activation redirect
    if ( empty( $redirect ) ) {
		return;
	}

	// Delete the redirect transient
	delete_site_transient( '_clusterpress_activation_redirect' );

	// Bail if bulk activated
	if ( isset( $_GET['activate-multi'] ) ) {
		return;
	}

	// Bail if the current user cannot see the about page
	if ( ! is_super_admin() ) {
		return;
	}

	$redirect_args = array(
		'page' => 'clusterpress-about',
	);

	if ( cp_is_install() ) {
		$redirect_args['is_install'] = 1;
	}

	// Redirect to ClusterPress about page
	wp_safe_redirect( add_query_arg( $redirect_args, self_admin_url( 'index.php' ) ) );
}
add_action( 'cp_activation_redirect', 'cp_about_clusterpress_redirect' );
