<?php

class CP_UnitTestCase extends WP_UnitTestCase {

	function setUp() {
		parent::setUp();

		if ( is_multisite() ) {
			clusterpress()->permalink_structure = '/%postname%/';
			$this->set_permalink_structure( '/%postname%/' );
		}
	}

	function tearDown() {
		parent::tearDown();

		if ( is_multisite() ) {
			clusterpress()->permalink_structure = '';
			$this->set_permalink_structure( '' );
		}
	}
}
