<?php
/**
 * ClusterPress Multisite Functions.
 *
 * @package ClusterPress\site\multisite
 * @subpackage functions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Capability callback to check if current user can access the sites archive.
 *
 * @since 1.0.0
 *
 * @param  int   $user_id The user ID.
 * @param  array $args    Adds the context to the cap. Typically the object ID.
 * @return array          The user's actual capabilities.
 */
function cp_sites_archive_required_caps( $user_id = 0, $args = array() ) {
	// Defaults to the users archive caps.
	$caps = cp_user_archive_required_caps( $user_id, $args );

	/**
	 * Filter here to edit the site archive caps
	 *
	 * @since 1.0.0
	 *
	 * @param  array $caps    The user's actual capabilities.
	 * @param  int   $user_id The user ID.
	 * @param  array $args    Adds the context to the cap. Typically the object ID.
	 */
	return apply_filters( 'cp_sites_archive_required_caps', $caps, $user_id, $args );
}

/**
 * Capability callback to check if current user can access a single site.
 *
 * @since 1.0.0
 *
 * @param  int   $user_id The user ID.
 * @param  array $args    Adds the context to the cap. Typically the object ID.
 * @return array          The user's actual capabilities.
 */
function cp_site_read_single_required_caps( $user_id = 0, $args = array() ) {
	$caps         = cp_user_get_default_caps();
	$network_type = cp_get_network_type();

	if ( ( 'private' === $network_type && ! empty( $user_id ) ) || ( 'public' === $network_type ) ) {
		$caps = array( 'exist' );
	}

	if ( 'closed' === $network_type && ! empty( $user_id ) && false !== array_search( cp_get_displayed_site_id(), cp_sites_get_user_sites( $user_id ) ) ) {
		$caps = array( 'exist' );
	}

	/**
	 * Filter here to edit the single site "read" caps
	 *
	 * @since 1.0.0
	 *
	 * @param  array $caps    The user's actual capabilities.
	 * @param  int   $user_id The user ID.
	 * @param  array $args    Adds the context to the cap. Typically the object ID.
	 */
	return apply_filters( 'cp_site_read_single_required_caps', $caps, $user_id, $args );
}

/**
 * Capability callback to check if current user can manage a single site.
 *
 * By default this part of the Site's discovery pages is restricted to Site Admins.
 *
 * @since 1.0.0
 *
 * @param  int   $user_id The user ID.
 * @param  array $args    Adds the context to the cap. Typically the object ID.
 * @return array          The user's actual capabilities.
 */
function cp_site_edit_single_required_caps( $user_id = 0, $args = array() ) {
	$caps = cp_user_get_default_caps();
	$site = null;

	if ( ! empty( $args[0]['site'] ) && is_a( $args[0]['site'], 'WP_Site' ) ) {
		$site = $args[0]['site'];

	} else {
		$site = cp_displayed_site();
	}

	if ( ! empty( $site->admins ) && false !== array_search( $user_id, $site->admins ) ) {
		$caps = array( 'exist' );
	}

	/**
	 * Filter here to edit the single site "manage" caps
	 *
	 * @since 1.0.0
	 *
	 * @param  array $caps    The user's actual capabilities.
	 * @param  int   $user_id The user ID.
	 * @param  array $args    Adds the context to the cap. Typically the object ID.
	 */
	return apply_filters( 'cp_site_read_single_required_caps', $caps, $user_id, $args );
}

/**
 * Add a user to a site as a member or as a follower.
 *
 * @since 1.0.0
 *
 * @param int    $user_id    The user ID.
 * @param int    $site_id    The site ID.
 * @param int    $user_level Whether it's a follower (0) or a member (from 1 to 5).
 * @param string $return     The expected type of return (boolean or the last inserted ID).
 * @return int|bool          The last inserted ID or a boolean: true on success, false otherwise.
 */
function cp_sites_add_user( $user_id = 0, $site_id = 0, $user_level = 0, $return = 'bool' ) {
	global $wpdb;
	$table = $wpdb->base_prefix . 'cp_user_sites';

	if ( empty( $site_id ) || empty( $user_id ) ) {
		return false;
	}

	$args = array(
		'user_id'    => (int) $user_id,
		'site_id'    => (int) $site_id,
		'user_level' => (int) $user_level,
	);

	$result = $wpdb->insert( $table, $args, array( '%d', '%d', '%d' ) );

	if ( $result ) {
		$action_part = 'followed';
		if ( 0 !== $args['user_level'] ) {
			$action_part = 'joined';
		}

		/**
		 * Hook here to perform custom actions once the user has been
		 * successfully added to the site.
		 *
		 * @since 1.0.0
		 *
		 * @param int $user_id    The user ID.
		 * @param int $site_id    The site ID.
		 * @param int $user_level Whether it's a follower (0) or a member (from 1 to 5).
		 */
		do_action_ref_array( "cp_sites_user_{$action_part}_site", $args );
	}

	if ( 'last_insert_id' === $return && ! empty( $wpdb->insert_id ) ) {
		return (int) $wpdb->insert_id;
	}

	return (bool) $result;
}

/**
 * Get a User/Site association using its ID.
 *
 * @since 1.0.0
 *
 * @param  int    $id     The ID of the User/Site association.
 * @param  string $return How the SQL query should return results. Defaults to 'OBJECT'.
 * @return array          The SQL query results.
 */
function cp_sites_get_user_by_id( $id = 0, $return = 'OBJECT' ) {
	global $wpdb;
	$table = $wpdb->base_prefix . 'cp_user_sites';

	if ( empty( $id ) ) {
		return false;
	}

	return $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$table} WHERE id = %d", $id ), $return );
}

/**
 * Remove a User/Site association using its ID.
 *
 * @since 1.0.0
 *
 * @param  int  $id The ID of the User/Site association.
 * @return bool True if the association was removed. False otherwise.
 */
function cp_sites_remove_user_by_id( $id = 0 ) {
	global $wpdb;
	$table = $wpdb->base_prefix . 'cp_user_sites';

	if ( empty( $id ) ) {
		return false;
	}

	$site_followed_relationship = cp_sites_get_user_by_id( $id, 'ARRAY_A' );

	if ( ! $site_followed_relationship ) {
		return false;
	} else {
		$site_followed_relationship = array_map( 'intval', $site_followed_relationship );
	}

	$deleted = $wpdb->delete( $table, array( 'id' => $id ), array( '%d' ) );

	if ( $deleted ) {
		$action_part = 'unfollowed';
		if ( 0 !== $site_followed_relationship['user_level'] ) {
			$action_part = 'left';
		}

		// Remove the id from the arguments keys.
		$args = array_diff_key( $site_followed_relationship, array( 'id' => false ) );

		/**
		 * Hook here to perform custom actions once the user/site association has been
		 * successfully removed.
		 *
		 * @since 1.0.0
		 *
		 * @param int $user_id    The user ID.
		 * @param int $site_id    The site ID.
		 * @param int $user_level Whether it's a follower (0) or a member (from 1 to 5).
		 */
		do_action_ref_array( "cp_sites_user_{$action_part}_site", $args );
	}

	return (bool) $deleted;
}

/**
 * Remove a user from a site.
 *
 * @since 1.0.0
 *
 * @param int       $user_id    The user ID.
 * @param int       $site_id    The site ID.
 * @param false|int $user_level The user's contribution level.
 * @return bool                 True on success. False otherwise.
 */
function cp_sites_remove_user( $user_id = 0, $site_id = 0, $user_level = false ) {
	global $wpdb;
	$table = $wpdb->base_prefix . 'cp_user_sites';

	if ( empty( $site_id ) && empty( $user_id ) ) {
		return false;
	}

	$args = array();
	if ( ! empty( $user_id ) ) {
		$args['user_id'] = (int) $user_id;
	}

	if ( ! empty( $site_id ) ) {
		$args['site_id'] = (int) $site_id;
	}

	if ( 'all' === $user_level ) {
		$where = array();

		foreach ( $args as $ka => $va ) {
			$where[] = $wpdb->prepare( "{$ka} = %d", $va );
		}

		// Keep followers.
		$where[] = 'user_level != 0';

		return (bool) $wpdb->query( "DELETE FROM {$table} WHERE " . join( ' AND ', $where ) );

	} elseif ( is_numeric( $user_level ) ) {
		$args['user_level'] = (int) $user_level;
	}

	$keys   = array_keys( $args );
	$format = array_fill_keys( $keys, '%d' );

	$deleted = $wpdb->delete( $table, $args, $format );

	if ( $deleted ) {
		$action_part = 'left';
		if ( isset( $args['user_level'] ) && 0 === $args['user_level'] ) {
			$action_part = 'unfollowed';
		}

		if ( ! empty( $args['user_id'] ) ) {
			/**
			 * Hook here to perform custom actions once the user has been
			 * successfully removed.
			 *
			 * @since 1.0.0
			 *
			 * @param int $user_id    The user ID.
			 * @param int $site_id    The site ID.
			 * @param int $user_level Whether it's a follower (0) or a member (from 1 to 5).
			 */
			do_action_ref_array( "cp_sites_user_{$action_part}_site", $args );
		}
	}

	return (bool) $deleted;
}

/**
 * Get the users for a given site.
 *
 * @since 1.0.0
 *
 * @param int    $site_id The site ID.
 * @param string $type Whether fetched users should be members or followers.
 * @param int    $user_level Whether it's a follower (0) or a member (from 1 to 5).
 */
function cp_sites_get_site_users( $site_id = 0, $type = 'members', $user_level = false ) {
	if ( empty( $site_id ) ) {
		return false;
	}

	global $wpdb;
	$table = $wpdb->base_prefix . 'cp_user_sites';

	$sql = array(
		'select' => "SELECT user_id FROM {$table}",
		'where'  => array(
			'site'       => $wpdb->prepare( 'site_id = %d', $site_id ),
			'user_level' => 'user_level > 0',
		),
	);

	if ( 'followers' === $type ) {
		$sql['where']['user_level'] = 'user_level = 0';
	}

	$sql['where'] = 'WHERE '  . join( ' AND ', $sql['where'] );

	if ( true === $user_level ) {
		$sql['select'] = "SELECT user_id, user_level FROM {$table}";

		return $wpdb->get_results( join( ' ', $sql ), OBJECT_K );
	}

	return $wpdb->get_col( join( ' ', $sql ) );
}

/**
 * Update the user's user level for a given site.
 *
 * @since 1.0.0
 *
 * @param int $user_id The user ID.
 * @param int $site_id The site ID.
 * @param int $user_level Whether it's a follower (0) or a member (from 1 to 5).
 */
function cp_sites_update_user_level( $user_id = 0, $site_id = 0, $user_level = 0 ) {
	global $wpdb;
	$table = $wpdb->base_prefix . 'cp_user_sites';

	if ( empty( $site_id ) || empty( $user_id ) ) {
		return false;
	}

	$args = array(
		'user_level' => (int) $user_level,
	);

	$where = array(
		'user_id'    => (int) $user_id,
		'site_id'    => (int) $site_id,
	);

	return (bool) $wpdb->update( $table, $args, $where, array( '%d', '%d' ), array( '%s' ) );
}

/**
 * Get a user's user level on a given site.
 *
 * @since 1.0.0
 *
 * @param int    $user_id The user ID.
 * @param int    $site_id The site ID.
 */
function cp_sites_get_user_level( $user_id = 0, $site_id = 0 ) {
	global $wpdb;
	$table = $wpdb->base_prefix . 'cp_user_sites';

	if ( empty( $site_id ) || empty( $user_id ) ) {
		return false;
	}

	$sql = array(
		'select' => "SELECT id, user_level FROM {$table}",
		'where'  => array(
			'site_id' => $wpdb->prepare( 'site_id = %d', $site_id ),
			'user_id' => $wpdb->prepare( 'user_id = %d', $user_id ),
		),
	);

	$sql['where'] = 'WHERE '  . join( ' AND ', $sql['where'] );

	return $wpdb->get_row( join( ' ', $sql ) );
}

/**
 * Get a list of sites.
 *
 * @since 1.0.0.
 *
 * @param $args {
 *    An array of arguments.
 *    @see CP_Site_Query::__construct() for a detailed description
 * }
 * @return array a list of WP_Site objects.
 */
function cp_get_sites( $args = array() ) {
	$r = wp_parse_args( $args, array(
		'count_total' => true,
	) );

	$sites = new CP_Site_Query( $r );

	if ( false === $r['count_total'] ) {
		return $sites->get_results();
	}

	$results = array(
		'sites' => $sites->get_results(),
	);

	if ( ! empty( $r['count_total'] ) ) {
		$results['total'] = $sites->get_total();
	}

	return $results;
}

/**
 * Update the Sites cache.
 *
 * @since 1.0.0
 *
 * @param WP_Site $site The site Object.
 */
function cp_site_update_cache( WP_Site $site ) {
	if ( empty( $site->blog_id ) ) {
		return;
	}

	$path_or_domain = $site->path;
	if ( is_subdomain_install() ) {
		$path_or_domain = $site->domain;
	}

	wp_cache_add( $site->blog_id,  $site,          'cp_site_ids'            );
	wp_cache_add( $path_or_domain, $site->blog_id, 'cp_site_path_or_domain' );
}

/**
 * Get a specific site using a specific key (ID, id or path).
 *
 * @since 1.0.0
 *
 * @param  string     $key   The field key (ID, id or path).
 * @param  int|string $value The site ID or the site path.
 * @return WP_Site           The Site object.
 */
function cp_get_site_by( $key = '', $value = '' ) {
	if ( empty( $key ) || empty( $value ) ) {
		return new WP_Error( 'Missing parameter', __( 'Vous avez besoin de fournir une clé et une valeur.', 'clusterpress' ) );
	}

	$site = null;

	// Init the query vars.
	$qv = array(
		'count_total' => false,
	);

	if ( 'path' === $key ) {
		$path = trim( $value, '/' );

		$current_network = get_network();

		// In case of subdomain configs, we look after a full domain
		if ( is_subdomain_install() ) {
			$path_or_domain   = $path . '.' . $current_network->domain;
			$qv['domain__in'] = array( $path_or_domain );

		// In case of subdirectory configs, we look after a path
		} else {
			$path_or_domain = $current_network->path . $path . '/';
			$qv['path__in'] = array( $path_or_domain );
		}

		$site_id = wp_cache_get( $path_or_domain, 'cp_site_path_or_domain' );

		if ( ! empty( $site_id ) ) {
			$qv['site__in'] = $site_id;
		}

	} elseif ( 'id' === $key || 'ID' === $key ) {
		$qv['site__in'] = (int) $value;

	} else {
		return new WP_Error( 'Wrong parameter', __( 'Vous devez utiliser les clés &quot;path&quot;, &quot;id&quot; ou &quot;ID&quot; pour obtenir un site.', 'clusterpress' ) );
	}

	if ( ! empty( $qv['site__in'] ) ) {
		$site = wp_cache_get( $qv['site__in'], 'cp_site_ids' );
	}

	if ( empty( $site->blog_id ) ) {
		$sites = cp_get_sites( $qv );
		$site  = reset( $sites );

		cp_site_update_cache( $site );
	}

	/**
	 * Filter here to add/edit site's properties.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Site    $site  The site object.
	 * @param string     $key   The field key (ID, id or path).
	 * @param int|string $value The site ID or the site path.
	 */
	return apply_filters( 'cp_get_site_by', $site, $key, $value );
}

/**
 * Get the URL to a site's discovery page (not the real home page!).
 *
 * @since 1.0.0
 *
 * @param $args {
 *   An array of arguments.
 *   @type string $rewrite_id The rewrite ID of the action.
 *   @type string $slug       The slug of the action.
 *   @type int    $site_id    The site ID.
 *   @type string $site_slug  The site slug.
 * }
 * @param  WP_Site The Site object (optional).
 * @return string  The discovery URL of the site.
 */
function cp_site_get_url( $args = array(), $site = null ) {
	global $wp_rewrite;

	$r = wp_parse_args( $args, array(
		'rewrite_id'    => '',
		'slug'          => '',
		'site_id'       => 0,
		'site_slug'     => '',
	) );

	// Bail if we can't get the corresponding user.
	if ( empty( $r['site_slug'] ) && empty( $r['site_id'] ) && is_null( $site ) ) {
		return false;
	}

	// Validate the site
	if ( ! is_a( $site, 'WP_Site' ) ) {
		if ( $r['site_id'] ) {
			$site = cp_get_site_by( 'id', $r['site_id'] );
		} else {
			$site = cp_get_site_by( 'path', $r['site_slug'] );
		}
	}

	// Bail if we haven't found the user.
	if ( empty( $site->blog_id ) ) {
		return false;
	}

	$site_slug       = '';
	$current_network = get_network();

	if ( is_subdomain_install() ) {
		$site_slug = trim( str_replace( $current_network->domain, '', $site->domain ), '.' );

	} else {
		$site_slug = trim( str_replace( $current_network->path, '', $site->path ), '/' );
	}

	// Check we're on the main site
	$is_main_site = cp_is_main_site();

	if ( ! $is_main_site ) {
		switch_to_blog( get_current_network_id() );
	}

	// Pretty permalinks
	if ( $wp_rewrite->using_permalinks() ) {
		$url = $wp_rewrite->root . trailingslashit( cp_get_root_slug() ) . cp_site_get_slug() . '/%' . cp_site_get_rewrite_id() . '%';

		$url = str_replace( '%' . cp_site_get_rewrite_id() . '%', $site_slug, $url );
		$url = home_url( trailingslashit( $url ) );

		if ( $r['slug'] ) {
			$url = trailingslashit( $url . $r['slug'] );
		}

	// Unpretty permalinks
	} else {
		$args = array( cp_site_get_rewrite_id() => $site_slug );

		if ( $r['rewrite_id'] ) {
			$args[ $r['rewrite_id'] ] = '1';
		}

		if ( ! empty( $r['slug'] ) ) {
			$parts = array_filter( explode( '/', $r['slug'] ) );

			if ( 1 !== count( $parts ) ) {
				$args[ $r['rewrite_id'] ] = $parts[1];
			}
		}

		$url = esc_url_raw( add_query_arg( $args, home_url( '/' ) ) );
	}

	if ( ! $is_main_site ) {
		restore_current_blog();
	}

	return $url;
}

/**
 * Get sites the user is associated to.
 * It can be all the sites or sites he has a specific user level on.
 *
 * @since 1.0.0
 *
 * @param int $user_id    The user ID.
 * @param int $user_level Whether it's a follower (0) or a member (from 1 to 5).
 * @param array           The list of Site IDs
 */
function cp_sites_get_user_sites( $user_id = 0, $user_level = false ) {
	// We should probably return array( 1 )!
	if ( empty( $user_id ) || ! is_multisite() ) {
		return false;
	}

	global $wpdb;
	$table = $wpdb->base_prefix . 'cp_user_sites';

	// Default is Sites the user has a role on.
	$sql = array(
		'select' => "SELECT site_id FROM {$table}",
		'where'  => array(
			'user_id'    => $wpdb->prepare( 'user_id = %d', $user_id ),
			'user_level' => 'user_level != 0'
		),
	);

	// Get sites for a specific level.
	if ( is_numeric( $user_level ) ) {
		$sql['where']['user_level'] = $wpdb->prepare( 'user_level = %d', $user_level );

	// Get any of the user sites.
	} elseif( 'any' === $user_level ) {
		unset( $sql['where']['user_level'] );
	}

	$sql['where'] = 'WHERE ' . join( ' AND ', $sql['where'] );

	return array_unique( $wpdb->get_col( join( ' ', $sql ) ) );
}

/**
 * Get the list of available user levels.
 *
 * @since 1.0.0
 *
 * @return array The user levels.
 */
function cp_sites_get_user_levels() {
	/**
	 * Filter here to add new user levels.
	 *
	 * @since 1.0.0
	 *
	 * @param array $value the user levels.
	 */
	return apply_filters( 'cp_sites_get_user_levels', array(
		'administrator' => array(
			'capability' => 'manage_options',
			'user_level' => 1,
		),
		'editor' => array(
			'capability' => 'edit_pages',
			'user_level' => 2,
		),
		'author' => array(
			'capability' => 'publish_posts',
			'user_level' => 3,
		),
		'contributor' => array(
			'capability' => 'edit_posts',
			'user_level' => 4,
		),
		'subscriber' => array(
			'capability' => 'read',
			'user_level' => 5,
		),
	) );
}

/**
 * Listen to new created site to add the cluster's site data.
 *
 * @since  1.0.0
 *
 * @param  int       $site_id The ID of the site.
 * @param  array|int $user_id The ID of the user (the admin) or a list of admin ids.
 * @return bool               True on success. False otherwise.
 */
function cp_sites_add_site( $site_id = 0, $user_id = array() ) {
	if ( empty( $site_id ) ) {
		return false;
	}

	$needs_restore = false;
	if ( (int) $site_id !== (int) get_current_blog_id() ) {
		$needs_restore = true;
		switch_to_blog( $site_id );
	}

	$associate_user = ! empty( $user_id ) && ! is_array( $user_id );
	if ( $associate_user ) {
		$user_id = array( (int) $user_id );
	}

	$meta_keys = cp_sites_get_sitemeta_keys();

	foreach ( $meta_keys as $mo => $mm ) {
		if ( 'url' === $mo ) {
			$meta_value = site_url();
		} elseif ( 'admins' === $mo ) {
			$meta_value = $user_id;
		} elseif ( 'site_icon' === $mo ) {
			$meta_value = get_site_icon_url( 150 );
		} else {
			$meta_value = get_option( $mo );
		}

		if ( ! $meta_value && 'blog_public' !== $mo ) {
			continue;
		}

		// Add the sitemeta
		cp_sites_add_meta( $site_id, $mm, $meta_value, true );
	}

	// We may need to add the user association
	if ( $associate_user ) {
		cp_sites_add_user( reset( $user_id ), $site_id, 1 );
	}

	if ( $needs_restore ) {
		restore_current_blog();
	}
}

/**
 * Listen to user's role changes to update/create the association
 * with the site.
 *
 * @since  1.0.0
 *
 * @param  int     $user_id The ID of the user.
 * @param  string  $role    The user's WordPress role.
 * @return bool             True if the user association was updated/created.
 *                          False otherwise.
 */
function cp_sites_add_user_site( $user_id = 0, $role = '' ) {
	$user_levels = cp_sites_get_user_levels();
	$site_id     = get_current_blog_id();

	if ( ! isset( $user_levels[ $role ] ) || empty( $user_id ) ) {
		return false;
	}

	// If we were not able to add it, that's probably because he already is associated
	if ( ! cp_sites_add_user( $user_id, $site_id, $user_levels[ $role ]['user_level'] ) ) {
		cp_sites_update_user_level( $user_id, $site_id, $user_levels[ $role ]['user_level'] );
	}

	// Get site admins
	$admins = cp_sites_get_meta( $site_id, 'admins', true );
	if ( empty( $admins ) && ! is_array( $admins ) ) {
		$admins = array();
	}

	// Eventually Add the user to site admins.
	if ( 'manage_options' === $user_levels[ $role ]['capability'] ) {
		if ( empty( $admins ) ) {
			cp_sites_add_meta( $site_id, 'admins', array( (int) $user_id ), true );
		} else {
			$admins[] = (int) $user_id;
			cp_sites_update_meta( $site_id, 'admins', wp_parse_id_list( $admins ) );
		}

	// If the user was an admin and no more is, remove it from site admins.
	} else {
		$was_admin = array_search( $user_id, $admins );

		if ( false !== $was_admin ) {
			unset( $admins[ $was_admin ] );
			cp_sites_update_meta( $site_id, 'admins', wp_parse_id_list( $admins ) );
		}
	}

	/**
	 * Hook here to perform actions once the member (having a role on the site)
	 * has been associated with the site.
	 *
	 * @since 1.0.0
	 *
	 * @param  int     $site_id The ID of the site.
	 * @param  int     $user_id The ID of the user.
	 * @param  string  $role    The user's WordPress role.
	 */
	do_action( 'cp_sites_add_user_site', $site_id, $user_id, $role );
}

/**
 * Remove a newly created user from a site association
 *
 * When creating a user on multisite configs, WordPress is first
 * creating a regular user with a role, then it is deleting the
 * capabilities of the user to remove him from the site. We need
 * to remove this site's association.
 * @see  wpmu_create_user()
 *
 * @since  1.0.0
 *
 * @param  int  $user_id The User ID to remove
 * @return bool          True if the user was removed from blog. False otherwise.
 */
function cp_sites_remove_user_site( $user_id = 0 ) {
	$site_id = get_current_blog_id();

	// Get site admins
	$admins    = cp_sites_get_meta( $site_id, 'admins', true );
	$was_admin = array_search( $user_id, $admins );

	if ( false !== $was_admin ) {
		unset( $admins[ $was_admin ] );
		cp_sites_update_meta( $site_id, 'admins', wp_parse_id_list( $admins ) );

		/**
		 * Hook here to perform actions once an Administrator
		 * has been removed from the site.
		 *
		 * @since 1.0.0
		 *
		 * @param  int     $site_id The ID of the site.
		 * @param  int     $user_id The ID of the user.
		 */
		do_action( 'cp_sites_remove_admin_from_site', $site_id, $user_id );
	}

	return cp_sites_remove_user( $user_id, $site_id );
}

/**
 * Get the categories assigned to a Site.
 *
 * @since 1.0.0
 *
 * @param  WP_Site $site   The site Object.
 * @param  string  $fields The list of fields to get (all or only ids).
 * @return array           The list of site category ids.
 */
function cp_sites_get_categories( $site = null, $fields = 'all' ) {
	$categories = array();

	if ( empty( $site->blog_id ) ) {
		return $categories;
	}

	$categories = wp_get_object_terms( $site->blog_id, cp_sites_get_taxonomy() );

	if ( is_wp_error( $categories ) ) {
		return array();
	}

	if ( 'ids' === $fields ) {
		$categories = wp_list_pluck( $categories, 'term_id' );
	}

	return $categories;
}

/**
 * Get the site category link.
 *
 * @since 1.0.0
 *
 * @param int $term_id The site category ID.
 * @return string      The url to filter the sites archive according to this specific category.
 */
function cp_sites_get_category_link( $term_id = 0 ) {
	if ( empty( $term_id ) ) {
		return '';
	}

	$link = get_term_link( $term_id, cp_sites_get_taxonomy() );

	if ( is_wp_error( $link ) ) {
		return '';
	}

	/**
	 * Filter here to edit the site category link.
	 *
	 * @since 1.0.0
	 *
	 * @param string $link    The url for the site category.
	 * @param int    $term_id The ID of the site category.
	 */
	return apply_filters( 'cp_sites_get_category_link', $link, $term_id );
}

/**
 * Get the list of assigned category links for a site.
 *
 * @since 1.0.0
 *
 * @param  WP_Site $site The Site object (required).
 * @return array         The list of assigned category links for a site.
 */
function cp_sites_get_site_category_list( $site = null ) {
	$list = array();
	$cp   = clusterpress();

	if ( empty( $site->blog_id ) ) {
		return $list;
	}

	// Populate the global if not already done.
	if ( empty( $cp->site->categories ) ) {
		$cp->site->categories = get_terms( array( 'taxonomy' => cp_sites_get_taxonomy(), 'hide_empty' => false ) );
	}

	// If there are no site categories no need to carry on.
	if ( empty( $cp->site->categories ) ) {
		return $list;
	}

	if ( ! isset( $site->categories ) ) {
		$site->categories = cp_sites_get_categories( $site, 'ids' );
	}

	// Remove any null value.
	$category_ids = array_filter( wp_parse_id_list( $site->categories ) );

	if ( empty( $category_ids ) ) {
		return $list;
	}

	foreach ( $cp->site->categories as $category ) {
		if ( ! in_array( $category->term_id, $category_ids ) ) {
			continue;
		}

		$category->href = cp_sites_get_category_link( $category->term_id );
		$category->link  = sprintf( '<a href="%1$s" rel="site-category">%2$s</a>',
			esc_url( $category->href ),
			esc_html( $category->name )
		);

		$list[] = $category;
	}

	return $list;
}

/**
 * Append user levels to the User's query results.
 *
 * @since 1.0.0
 *
 * @param  array the User's query results.
 * @return array the User's query results.
 */
function cp_site_user_query_append_levels( $results = array() ) {
	$site = cp_displayed_site();

	if ( empty( $site->users_level ) ) {
		return $results;
	}

	// Get levels to role map
	$levels_map = cp_sites_get_user_levels();

	foreach ( $results as $user_id => $user_data ) {
		if ( isset( $site->users_level[ $user_id ] ) ) {
			$role_map = wp_list_filter( $levels_map, array( 'user_level' => (int) $site->users_level[ $user_id ]->user_level ) );

			if ( is_array( $role_map ) && 1 === count( $role_map ) ) {
				$results[ $user_id ]->site_role = key( $role_map );
			}
		}
	}

	return $results;
}
add_filter( 'cp_user_query_results', 'cp_site_user_query_append_levels', 10, 1 );

/**
 * Adapt the Sites loop pagination to the context of the displayed page.
 *
 * @since 1.0.0
 *
 * @param array $paginate_args {
 *    An array of arguments.
 *    @see paginate_links() for a detailed description.
 * }
 * @return array The paginate args adapted to the context.
 */
function cp_site_users_loop_paginate_args( $paginate_args = array() ) {
	// We only need to edit the pretty links.
	if ( ! clusterpress()->permalink_structure ) {
		return $paginate_args;
	}

	// Followers
	if ( cp_is_site_followers() ) {
		$paginate_args['base']   = trailingslashit( cp_site_get_url( array(
			'rewrite_id'    => cp_site_get_followers_rewrite_id(),
			'slug'          => cp_site_get_followers_slug(),
		), cp_displayed_site() ) ) . '%_%';

	// Members
	} elseif ( cp_is_site_members() ) {
		$paginate_args['base']   = trailingslashit( cp_site_get_url( array(
			'rewrite_id'    => cp_site_get_members_rewrite_id(),
			'slug'          => cp_site_get_members_slug(),
		), cp_displayed_site() ) ) . '%_%';

	// Manage Followers
	} elseif ( cp_is_site_manage_followers() ) {
		$paginate_args['base']   = trailingslashit( cp_site_get_url( array(
			'rewrite_id'    => cp_site_get_manage_rewrite_id(),
			'slug'          => cp_site_get_manage_slug() . '/' . cp_site_get_followers_slug(),
		), cp_displayed_site() ) ) . '%_%';

	// Manage Members
	} elseif ( cp_is_site_manage_members() ) {
		$paginate_args['base']   = trailingslashit( cp_site_get_url( array(
			'rewrite_id'    => cp_site_get_manage_rewrite_id(),
			'slug'          => cp_site_get_manage_slug() . '/' .  cp_site_get_members_slug(),
		), cp_displayed_site() ) ) . '%_%';
	}

	return $paginate_args;
}
add_filter( 'cp_users_loop_paginate_args', 'cp_site_users_loop_paginate_args', 10, 1 );

/**
 * Adapt the user form action to the site management context.
 *
 * @since 1.0.0
 *
 * @param  string $url The form action.
 * @return string      The form action adapted to the context.
 */
function cp_site_user_form_action( $url = '' ) {
	$site = cp_displayed_site();

	if ( empty( $site->blog_id ) ) {
		return $url;
	}

	// Manage Followers
	if ( cp_is_site_manage_followers() ) {
		$url = trailingslashit( cp_site_get_url( array(
			'rewrite_id'    => cp_site_get_manage_rewrite_id(),
			'slug'          => cp_site_get_manage_slug() . '/' . cp_site_get_followers_slug(),
		), $site ) );

	// Manage Members
	} elseif ( cp_is_site_manage_members() ) {
		$url = trailingslashit( cp_site_get_url( array(
			'rewrite_id'    => cp_site_get_manage_rewrite_id(),
			'slug'          => cp_site_get_manage_slug() . '/' .  cp_site_get_members_slug(),
		), $site ) );
	}

	return $url;
}
add_filter( 'cp_user_form_action', 'cp_site_user_form_action' );

/**
 * Add a no robots meta tag to the single site's header if needed.
 *
 * @since 1.0.0
 */
function cp_site_not_public_header_meta() {
	if ( ! cp_displayed_site() ) {
		return;
	}

	$site = cp_displayed_site();
	if ( isset( $site->public ) && 0 === (int) $site->public ) {
		wp_no_robots();
	}
}
