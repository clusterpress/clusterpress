<?php
/**
 * Actions specific to the Site following feature.
 *
 * @package ClusterPress\site\multisite\following-sites
 * @subpackage actions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Register the Following Sites sections.
 *
 * @since 1.0.0
 */
function cp_sites_followed_register_user_section() {

	cp_register_cluster_section( 'user', array(
		'id'            => 'followed_sites',
		'name'          => __( 'Sites suivis', 'clusterpress' ),
		'cluster_id'    => 'user',
		'slug'          => cp_site_get_user_following_slug(),
		'rewrite_id'    => cp_site_get_user_following_rewrite_id(),
		'template'      => '',
		'template_dir'  => '',
		'toolbar_items' => array(
			'position'   => 70,
			'dashicon'   => 'dashicons-tickets-alt',
			'capability' => 'cp_edit_single_user',
		),
	) );

	cp_register_cluster_section( 'user', array(
		'id'            => 'manage_followed_sites',
		'name'          => __( 'Sites suivis', 'clusterpress' ),
		'cluster_id'    => 'user',
		'slug'          => cp_site_get_user_manage_followed_slug(),
		'rewrite_id'    => cp_site_get_user_manage_followed_rewrite_id(),
		'template'      => '',
		'template_dir'  => '',
		'toolbar_items' => array(
			'position'   => 30,
			'capability' => 'cp_edit_single_user',
			'parent'     => cp_user_get_manage_rewrite_id(),
		),
	) );
}
add_action( 'cp_register_cluster_sections', 'cp_sites_followed_register_user_section', 12 );

/**
 * Register the Following Sites relationships.
 *
 * @since 1.0.0
 */
function cp_sites_followed_register_relationships() {
	cp_user_register_relationship( 'user', array(
		'name'             => 'followed_site_new_post',
		'cluster'          => 'site',
		'singular'         => __( 'Dernier article', 'clusterpress'  ),
		'plural'           => __( 'Derniers articles', 'clusterpress' ),
		'description'      => __( 'Derniers articles pour le site suivi', 'clusterpress' ),
		'format_callback'  => __( 'Un nouvel article a été publié sur le site %1$s %2$s', 'clusterpress' ),
	) );
}
add_action( 'cp_register_relationships', 'cp_sites_followed_register_relationships' );

/**
 * Add/delete pings about new posts of followed sites when a post is published/updated/trashed.
 *
 * NB: This is happening only if the user is not already mentioned in the post.
 *
 * @since 1.0.0
 *
 * @param string  $new_status The updated status for the Post .
 * @param string  $old_status The previous status of the Post.
 * @param WP_Post $object     The Post object.
 */
function cp_sites_followed_site_new_post( $new_status = '', $old_status = '', $post = null ) {
	if ( empty( $post ) ) {
		return;
	}

	if ( ! is_a( $post, 'WP_Post' ) || ! cp_user_relationships_isset() || ! post_type_supports( $post->post_type, 'clusterpress_site_followed_posts' ) ) {
		return;
	}

	$primary_id   = $post->ID;
	$secondary_id = (int) get_current_blog_id();

	if ( 'publish' !== $new_status || 'publish' === $old_status ) {
		return cp_sites_delete_site_followed_content( array(
			'primary_id'   => (int) $primary_id,
		) );
	}

	$followers = cp_sites_get_site_users( $secondary_id, 'followers' );

	if ( empty( $followers ) ) {
		return;
	}

	$cp = clusterpress();

	foreach ( $followers as $follower_id ) {
		// Make sure the user is not already noticed for the same content.
		$already_noticed = $cp->user->relationships->get( array(
			'id',
		), array(
			'user_id'      => $follower_id,
			'primary_id'   => $primary_id,
			'secondary_id' => $secondary_id,
			'name'         => array( 'post_mentions', 'followed_site_new_post' ),
		) );

		if ( ! empty( $already_noticed ) ) {
			continue;
		}

		cp_user_add_relationship( array(
			'user_id'      => $follower_id,
			'primary_id'   => $primary_id,
			'secondary_id' => $secondary_id,
			'name'         => 'followed_site_new_post',
			'date'         => $post->post_date_gmt,
		) );

		/**
		 * Hook here if you need to perform custom actions when the relationship has been registered.
		 * (eg: send an email notification)
		 *
		 * @since 1.0.0
		 *
		 * @param int     $follower_id The follower user ID.
		 * @param WP_Post $post        The Post object.
		 */
		do_action( 'cp_sites_followed_site_new_post', $follower_id, $post );
	}
}
add_action( 'transition_post_status', 'cp_sites_followed_site_new_post', 15, 3 );

/**
 * Handle User's followed sites actions.
 *
 * @since 1.0.0
 */
function cp_sites_handle_user_actions() {
	if ( ! cp_is_user_followed_sites() ) {
		return;
	}

	if ( ! empty( $_GET['redirectto'] ) || ! empty( $_GET['read'] ) || ! empty( $_GET['readall'] ) ) {
		$relationship_id = 0;
		$redirect        = remove_query_arg( array( 'redirectto', 'read', '_wpnonce', 'readall' ), wp_get_referer() );
		$user_id         = cp_get_displayed_user_id();

		if ( ! empty( $_GET['redirectto'] ) ) {
			$relationship_id = (int) $_GET['redirectto'];

			check_admin_referer( 'cp_view_followed_site_post' );

		} elseif ( ! empty( $_GET['read'] ) ) {
			$relationship_id = (int) $_GET['read'];

			check_admin_referer( 'cp_read_followed_site_post' );

		// Bulk read all is done right away.
		} elseif ( ! empty( $_GET['readall'] ) ) {

			check_admin_referer( 'cp_sites_followed_bulk_read' );

			if ( ! cp_user_is_self_profile() ) {
				cp_feedback_redirect( 'site-followed-04', 'error', $redirect );
			}

			if ( ! cp_user_remove_relationship( array(
				'user_id' => $user_id,
				'name'    => 'followed_site_new_post',
			) ) ) {
				cp_feedback_redirect( 'site-followed-05', 'error', $redirect );
			}

			/**
			 * Hook here if you need to perform custom actions once all pings are read
			 *
			 * @since 1.0.0
			 *
			 * @param int $user_id The follower user ID.
			 */
			do_action( 'cp_sites_followed_all_read', $user_id );

			cp_feedback_redirect( 'site-followed-06', 'updated', $redirect );
		}

		$post = clusterpress()->user->relationships->get( array(
			'id',
			'user_id',
			'primary_id',
			'secondary_id',
			'name',
			'date',
		), array(
			'id' => $relationship_id,
		) );

		if ( empty( $post ) || ! cp_user_is_self_profile() ) {
			cp_feedback_redirect( 'site-followed-01', 'error', $redirect );
		} else {
			$post = reset( $post );
		}

		if ( ! cp_user_remove_relationship( array( 'id' => $relationship_id ) ) ) {
			cp_feedback_redirect( 'site-followed-02', 'error', $redirect );
		}

		/**
		 * Hook here if you need to perform custom actions once a ping is read.
		 *
		 * @since 1.0.0
		 *
		 * @param int    $user_id The follower user ID.
		 * @param object $post    The relationship object.
		 */
		do_action( 'cp_sites_followed_read', $user_id, $post );

		if ( ! empty( $_GET['read'] ) ) {
			cp_feedback_redirect( 'site-followed-03', 'updated', $redirect );
		}

		$is_main_site = cp_is_main_site( $post->secondary_id );

		if ( ! $is_main_site ) {
			switch_to_blog( $post->secondary_id );
		}

		// Finally redirect users to the post.
		$redirect = get_permalink( $post->primary_id );

		if ( ! $is_main_site ) {
			restore_current_blog();
		}

		wp_redirect( $redirect );
		exit();
	}
}
add_action( 'cp_page_actions', 'cp_sites_handle_user_actions' );

/**
 * Remove a user's new posts notifications for a given site.
 *
 * @since 1.0.0
 *
 * @param int $user_id The user ID who unfollowed the site.
 * @param int $site_id The unfollowed site ID.
 */
function cp_sites_followed_remove_unread_posts( $user_id = 0, $site_id = 0 ) {
	if ( empty( $user_id ) || empty( $site_id ) ) {
		return;
	}

	if ( cp_user_remove_relationship( array(
		'user_id'      => $user_id,
		'secondary_id' => $site_id,
		'name'         => 'followed_site_new_post',
	) ) ) {
		/**
		 * Hook here if you need to perform custom actions when the user unfollowed a site.
		 *
		 * @since 1.0.0
		 *
		 * @param int $user_id The user ID who unfollowed the site.
		 * @param int $site_id The unfollowed site ID.
		 */
		do_action( 'cp_sites_followed_unread_removed', $user_id, $site_id );
	}
}
add_action( 'cp_sites_user_unfollowed_site', 'cp_sites_followed_remove_unread_posts', 10, 2 );

/**
 * Clean the new unread posts cache for a given user.
 *
 * @since 1.0.0
 *
 * @param int $user_id The user ID.
 */
function cp_sites_followed_reset_unread_cache( $user_id = 0 ) {
	wp_cache_delete( $user_id, 'cp_site_followed_new_posts' );
}
add_action( 'cp_sites_followed_site_new_post',                'cp_sites_followed_reset_unread_cache', 10, 1 );
add_action( 'cp_sites_followed_all_read',                     'cp_sites_followed_reset_unread_cache', 10, 1 );
add_action( 'cp_sites_followed_read',                         'cp_sites_followed_reset_unread_cache', 10, 1 );
add_action( 'cp_sites_followed_unread_removed',               'cp_sites_followed_reset_unread_cache', 10, 1 );
add_action( 'cp_sites_delete_site_followed_content_for_user', 'cp_sites_followed_reset_unread_cache', 10, 1 );

/**
 * Remove all users site's following data when a site is deleted.
 *
 * @since 1.0.0
 *
 * @param int $site_id The deleted site ID.
 */
function cp_sites_remove_site_followed_data_for_site( $site_id = 0 ) {
	if ( empty( $site_id ) ) {
		return;
	}

	// Remove relationships to new content.
	cp_sites_delete_site_followed_content( array(
		'secondary_id' => $site_id,
	) );

	$followers = cp_sites_get_site_users( $site_id, 'followers' );

	// Delete cache for all followers
	if ( ! empty( $followers ) ) {
		foreach ( $followers as $follower_id ) {
			cp_sites_followed_reset_unread_cache( $follower_id );
		}
	}

	// Remove the user's association.
	cp_sites_remove_user( false, $site_id, 0 );

	return true;
}
add_action( 'delete_blog', 'cp_sites_remove_site_followed_data_for_site', 10, 1 );
