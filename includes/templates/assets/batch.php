<?php
/**
 * ClusterPress Batch template.
 *
 * @package ClusterPress\templates\assets
 * @subpackage batch
 *
 * @since 1.0.0
 */
?>

<div id="clusterpress-batch"></div>

<script type="text/html" id="tmpl-progress-window">
	<div id="{{data.id}}">
		<div class="task-description">{{data.message}}</div>
		<div class="clusterpress-task-progress">
			<div class="clusterpress-task-bar"></div>
		</div>
	</div>
</script>
