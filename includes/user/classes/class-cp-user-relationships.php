<?php
/**
 * ClusterPress User Relationships.
 *
 * @package ClusterPress\user\classes
 * @subpackage user-relationships
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * User content relationships class.
 *
 * @since 1.0.0
 */
class CP_User_Relationships {

	/**
	 * Name of the relationships table.
	 *
	 * @var string
	 */
	public $table = '';

	/**
	 * Available relationship types.
	 *
	 * @var array
	 */
	public $types = array();

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args {
	 *    An array of arguments.
	 *    @type string $cluster      The Cluster ID.
	 *    @type array  $relationship The relationship arguments.
	 * }
	 */
	public function __construct( $args = array() ) {
		global $wpdb;

		$this->table = $wpdb->base_prefix . 'cp_relationships';

		// Let's make sure we'll be the first to register the last_active type :)
		if ( ! empty( $args['cluster'] ) && ! empty( $args['relationship'] ) ) {
			$this->register_type( $args['cluster'], $args['relationship'] );
		}
	}

	/**
	 * Register multiple relationship types, making sure they are unique
	 *
	 * @since 1.0.0
	 *
	 * @param  string      $cluster The cluster ID
	 * @param  array       $types   The list of relationships type to add
	 * @return bool|array  True if the relationship was registered. An array
	 *                     containing the unregistered types otherwise.
	 */
	public function register_types( $cluster, $types = array() ) {
		$errors = array();

		foreach ( (array) $types as $type ) {
			if ( ! $this->register_type( $cluster, $type ) ) {
				$errors[] = $type;
			}
		}

		if ( empty( $errors ) ) {
			return true;
		}

		return $errors;
	}

	/**
	 * Register a single relationship type, making sure it is unique
	 *
	 * @since 1.0.0
	 *
	 * @param string $cluster The cluster ID
	 * @param array  $type {
	 *    An array of arguments.
	 *    @type string $name            The name of the relationship.
	 *    @type string $cluster         The Cluster ID.
	 *    @type string $singular        The singular label for the relationship.
	 *    @type string $plural          The plural label for the relationship.
	 *    @type string $description     The description of the relationship.
	 *    @type array  $format_callback An associative array containing strings to format the output.
	 * }
	 * @return bool  True if the relationship was registered. False otherwise.
	 */
	public function register_type( $cluster, $type = array() ) {
		if ( empty( $type['name'] ) ) {
			return false;
		}

		$type['name'] = sanitize_key( $type['name'] );

		if ( isset( $this->types[ $type['name'] ] ) ) {
			return false;
		}

		$this->types[ $type['name'] ] =  (object) array_intersect_key( $type, array(
			'name'             => '',
			'cluster'          => $cluster,
			'singular'         => '',
			'plural'           => '',
			'description'      => '',
			'format_callback'  => '',
		) );

		return true;
	}

	/**
	 * Unregister a Relationship Type.
	 *
	 * @since 1.0.0
	 *
	 * @param  string $name The name of the relationship type.
	 * @return bool         True if the relationship is unregistered. False Otherwise.
	 */
	public function unregister_type( $name = '' ) {
		if ( empty( $name ) || ! $this->get_relationship_object( $name ) ) {
			return false;
		}

		unset( $this->types[ $name ] );
		return true;
	}

	/**
	 * Get a Relationship Type object.
	 *
	 * @since 1.0.0
	 *
	 * @param  string       $name The name of the relationship type.
	 * @return false|object       The relationship object if set. False Otherwise.
	 */
	public function get_relationship_object( $name = '' ) {
		if ( empty( $name ) ) {
			return false;
		}

		$name = sanitize_key( $name );

		if ( ! isset( $this->types[ $name ] ) ) {
			return false;
		}

		return $this->types[ $name ];
	}

	/**
 	 * Add a relationship entry for the user.
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args The query arguments {
	 *    An array of arguments
	 *    @type int    $user_id      The user ID.
	 *    @type int    $primary_id   The primary content ID (eg: Post).
	 *    @type int    $secondary_id The secondary content ID (eg: Site).
	 *    @type string $name         The name of the relationship.
	 *    @type string $date         The date the relationship was saved.
	 * }
	 * @return bool True on success, false otherwise
	 */
	public function add( $args = array() ) {
		global $wpdb;

		$r = array_intersect_key( $args, array(
			'user_id'      => 0,
			'primary_id'   => 0,
			'secondary_id' => 0,
			'name'         => '',
			'date'         => '',
		) );

		if ( empty( $r['user_id'] ) || empty( $r['primary_id'] ) || empty( $r['name'] ) ) {
			return false;
		}

		if ( ! isset( $this->types[ $r['name'] ] ) ) {
			return false;
		}

		$added = $wpdb->insert(
			$this->table,
			$r
		);

		if ( ! empty( $args['last_insert_id'] ) ) {
			return (int) $wpdb->insert_id;
		}

		return (bool) $added;
	}

	/**
 	 * Update a relationship entry for the user.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args The fields to update {
	 *    An array of arguments
	 *    @type int    $secondary_id The secondary content ID (eg: Site).
	 *    @type string $date         The date the relationship was saved.
	 * }
	 * @param array $where The query to match {
	 *    An array of arguments
	 *    @type int    $id           The relationship ID.
	 *    @type int    $user_id      The user ID.
	 *    @type int    $primary_id   The primary content ID (eg: Post).
	 *    @type string $name         The name of the relationship.
	 *    @type string $date         The date the relationship was saved.
	 * }
	 * @return bool True on success, false otherwise
	 */
	public function update( $args = array(), $where = array() ) {
		global $wpdb;

		$r = array_intersect_key( $args, array(
			'secondary_id' => null,
			'date'         => null,
		) );

		$w = array_intersect_key( $where, array(
			'id'           => 0,
			'user_id'      => 0,
			'primary_id'   => 0,
			'name'         => '',
			'date'         => '',
		) );

		if ( empty( $w['id'] ) && ( empty( $w['user_id'] ) || empty( $w['name'] ) ) && ( empty( $w['primary_id'] ) || empty( $w['name'] ) ) ) {
			return false;
		}

		if ( empty( $w['name'] ) || ! isset( $this->types[ $w['name'] ] ) ) {
			return false;
		}

		$updated = $wpdb->update(
			$this->table,
			$r,
			$w
		);

		return (bool) $updated;
	}

	/**
	 * Get the last active date of a user.
	 *
	 * @since 1.0.0
	 *
	 * @param  int    $user_id The user ID.
	 * @return string $value   The last active date of the user.
	 */
	public function get_last_active( $user_id = 0 ) {
		global $wpdb;

		return $wpdb->get_var( $wpdb->prepare( "SELECT date FROM {$this->table} WHERE name = 'last_active' AND user_id = %d", $user_id ) );
	}

	/**
	 * Get the last active date for a specified list of user ids.
	 *
	 * @since 1.0.0
	 *
	 * @param  array $user_ids The list of user IDs.
	 * @return array           The list of last active date by user_id.
	 */
	public function get_last_actives( $user_ids = array() ) {
		global $wpdb;

		$in = $this->set_clause( 'user_id', $user_ids );

		return $wpdb->get_results( "SELECT user_id, date FROM {$this->table} WHERE {$in} AND name = 'last_active'" );
	}

	/**
	 * Set Where clauses of the query
	 *
	 * @since 1.0.0
	 *
	 * @param  string $in_field Name of the field
	 * @param  array  $in List of object/item IDs
	 * @return string The where clause.
	 */
	public function set_clause( $field_name = '', $in = array() ) {
		global $wpdb;

		if ( empty( $field_name ) || empty( $in ) ) {
			return;
		}

		if ( ! is_array( $in ) ) {
			$in = (array) $in;
		}

		$count = count( $in );

		if ( $field_name === 'date' || $field_name === 'name' ) {
			if ( 1 === $count ) {
				$s = reset( $in );

				if ( is_null( $s ) || 'null' === $s ) {
					return "{$field_name} IS NULL";
				} else {
					return $wpdb->prepare( "{$field_name} = %s", $s );
				}

			} else {
				$in = array_map( 'esc_sql', $in );
				$in = '"' . join( '","', $in ) . '"';
				return "{$field_name} IN ({$in})";
			}
		} else {
			if ( 1 === $count ) {
				$d = reset( $in );

				if ( is_null( $d ) || 'null' === $d ) {
					return "{$field_name} IS NULL";
				} elseif ( 'exclude' === $field_name ) {
					return $wpdb->prepare( "id != %d", $d );
				} else {
					return $wpdb->prepare( "{$field_name} = %d", $d );
				}

			} else {
				$in = wp_parse_id_list( $in );
				$in = join( ',', $in );

				$operator = 'IN';
				$field    = $field_name;

				if ( 'exclude' === $field_name ) {
					$operator = 'NOT IN';
					$field    = 'id';
				}

				return "{$field} {$operator} ({$in})";
			}

		}
	}

	/**
	 * Get a column or columns values matching the clauses
	 *
	 * @since 1.0.0
	 *
	 * @param  string|array $column The name of the column(s) to get in the results.
	 * @param  array        $args   The query arguments {
	 *    An array of arguments
	 *    @type int    $user_id      The user ID.
	 *    @type int    $primary_id   The primary content ID (eg: Post).
	 *    @type int    $secondary_id The secondary content ID (eg: Site).
	 *    @type string $name         The name of the relationship.
	 *    @type string $date         The date the relationship was saved.
	 *    @type array  $exclude      A list of relationship IDs to exclude from results.
	 * }
	 * @param int|false $offset      Specify the offset or false to get any.
	 * @param int|false $number      Specify the number to limit results or false to get any.
	 * @param bool      $desc        Order ASC by ID if false. Order DESC by ID otherwise.
	 * @return array                 List of values
	 */
	public function get( $column = '', $args = array(), $offset = false, $number = false, $desc = false ) {
		global $wpdb;

		if ( empty( $column ) || empty( $args ) ) {
			return false;
		}

		$type = 'col';
		if ( is_array( $column ) ) {
			$type = 'results';
			$column = join( ', ', $column );
		}

		$r = array_intersect_key( (array) $args, array(
			'id'           => 0,
			'user_id'      => 0,
			'primary_id'   => 0,
			'secondary_id' => 0,
			'name'         => '',
			'date'         => '',
			'exclude'      => array(),
		) );

		$sql = array(
			'select' => sprintf( 'SELECT %s FROM %s', esc_sql( $column ), $this->table ),
			'where'  => array(),
		);

		if ( $desc ) {
			$sql['order'] = 'ORDER BY id DESC';
		}

		if ( false !== $offset && false !== $number ) {
			$sql['limit'] = $wpdb->prepare( 'LIMIT %d, %d', $offset, $number );
		}

		foreach ( $r as $field_name => $in ) {
			$sql['where'][] = $this->set_clause( $field_name, $in );
		}

		$sql['where'] = 'WHERE ' . join( ' AND ', $sql['where'] );

		if ( 'col' === $type ) {
			return $wpdb->get_col( join( ' ', $sql ) );
		}

		return $wpdb->get_results( join( ' ', $sql ) );
	}

	/**
	 * Get the count of a specific relationship query.
	 *
	 * @since 1.0.0
	 *
	 * @param  string|array $column The name of the column to count.
	 * @param  array        $args   The query arguments {
	 *    An array of arguments
	 *    @type int    $user_id      The user ID.
	 *    @type int    $primary_id   The primary content ID (eg: Post).
	 *    @type int    $secondary_id The secondary content ID (eg: Site).
	 *    @type string $name         The name of the relationship.
	 *    @type string $date         The date the relationship was saved.
	 *    @type array  $exclude      A list of relationship IDs to exclude from results.
	 * }
	 * @param  false|string $group_by False to not group the count, one of these field names to
	 *                                group the count: 'user_id', 'primary_id', 'secondary_id'.
	 * @return array|int List of counted values grouped by a specific field or the query count.
	 */
	public function count( $column = '', $args = array(), $group_by = false ) {
		global $wpdb;

		if ( empty( $column ) || empty( $args ) ) {
			return false;
		}

		if ( is_array( $column ) ) {
			$column = reset( $column );
		}

		$r = array_intersect_key( (array) $args, array(
			'user_id'      => 0,
			'primary_id'   => 0,
			'secondary_id' => 0,
			'name'         => '',
			'date'         => '',
			'exclude'      => array(),
		) );

		$sql = array(
			'select' => sprintf( 'SELECT count( %s ) FROM %s', esc_sql( $column ), $this->table ),
			'where'  => array(),
		);

		foreach ( $r as $field_name => $in ) {
			$sql['where'][] = $this->set_clause( $field_name, $in );
		}

		$sql['where'] = 'WHERE ' . join( ' AND ', $sql['where'] );

		if ( $group_by && in_array( $group_by, array( 'user_id', 'primary_id', 'secondary_id' ) ) ) {
			$sql['select']  = sprintf( 'SELECT DISTINCT %s, count( %s ) as count FROM %s', esc_sql( $group_by ), esc_sql( $column ), $this->table );
			$sql['groupby'] = sprintf( 'GROUP BY %s', esc_sql( $group_by ) );
			$sql['orderby'] = 'ORDER BY count DESC';

			return $wpdb->get_results( join( ' ', $sql ), OBJECT_K );
		}

		return $wpdb->get_var( join( ' ', $sql ) );
	}

	/**
 	 * Remove item ids for a given object id or vice versa
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args   The query arguments {
	 *    An array of arguments
	 *    @type int    $id           The relationship ID.
	 *    @type int    $user_id      The user ID.
	 *    @type int    $primary_id   The primary content ID (eg: Post).
	 *    @type int    $secondary_id The secondary content ID (eg: Site).
	 *    @type string $name         The name of the relationship.
	 *    @type string $date         The date the relationship was saved.
	 * }
	 * @return array List of item ids or object ids
	 */
	public function remove( $args = array() ) {
		global $wpdb;

		if ( empty( $args ) ) {
			return false;
		}

		$r = array_intersect_key( (array) $args, array(
			'id'           => 0,
			'user_id'      => 0,
			'primary_id'   => 0,
			'secondary_id' => 0,
			'name'         => '',
			'date'         => '',
		) );

		if ( empty( $r['id'] ) ) {

			if ( empty( $r['name'] ) || ! isset( $this->types[ $r['name'] ] ) ) {
				return false;
			}

			if ( empty( $r['user_id'] ) && empty( $r['primary_id'] ) && empty( $r['secondary_id'] ) ) {
				return false;
			}
		}

		$sql = array(
			'select' => sprintf( 'DELETE FROM %s', $this->table ),
		);

		foreach ( $r as $field_name => $in ) {
			$sql['where'][] = $this->set_clause( $field_name, $in );
		}

		$sql['where'] = 'WHERE ' . join( ' AND ', $sql['where'] );

		return (bool) $wpdb->query( join( ' ', $sql ) );
	}
}
