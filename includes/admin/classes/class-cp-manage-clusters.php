<?php
/**
 * Clusters Manager
 *
 * @since  1.0.0
 *
 * @package    ClusterPress\Admin\classes
 * @subpackage manage-clusters
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Class extending the WordPress Plugin List table to manage
 * built-in and custom Clusters.
 *
 * @since  1.0.0
 */
class CP_Manage_Clusters extends WP_Plugin_Install_List_Table {

	/**
	 * Prepare the Clusters list for display.
	 *
	 * @since 1.0.0
	 */
	public function prepare_items() {
		global $tab;
		$enabled_clusters = array_flip( clusterpress()->active_clusters );

		include( ABSPATH . 'wp-admin/includes/plugin-install.php' );

		// These are the tabs which are shown on the page
		clusterpress()->admin->manage_clusters_tabs = array(
			'all'           => _x( 'Tous', 'Plugin Installer', 'clusterpress' ),
			'cluster.press' => _x( 'Disponibles sur Cluster.Press', 'Plugin Installer', 'clusterpress' ),
		);

		$tab = 'all';

		if ( ! empty( $_GET['tab'] ) ) {
			$tab = $_GET['tab'];
		}

		if ( 'cluster.press' === $tab ) {
			$active_clusters = array_merge( array( 'clusterpress' ), cp_admin_get_custom_clusters( '', 'slugs' ) );
			add_filter( 'plugins_api', 'cp_clusters_api', 10, 3 );
			$api = plugins_api( 'query_clusters', array(
				'page'               => $this->get_pagenum(),
				'per_page'           => 10,
				'installed_clusters' => $active_clusters,
			) );

			if ( is_wp_error( $api ) ) {
				$this->error = $api;
				return;
			}

			$this->items = array();
			$total       = 0;

			if ( ! empty( $api->plugins ) ) {
				$this->items = (array) $api->plugins;
				$total       = $api->results;
			}

		} else {
			$this->items = cp_admin_get_clusters();
			$total       = count( $this->items );
		}

		$this->set_pagination_args( array(
			'total_items' => $total,
			'per_page'    => 10,
		) );
	}

	/**
	 * Output 'no Clusters' message.
	 *
	 * @since 1.0.0
	 */
	public function no_items() {
		if ( isset( $this->error ) ) {
			$message = $this->error->get_error_message() . '<p class="hide-if-no-js"><a href="#" class="button" onclick="document.location.reload(); return false;">' . __( 'Essayez encore.', 'clusterpress' ) . '</a></p>';
		} else {
			$message = __( 'Aucun nouveau cluster n\'est disponible par rapport à votre configuration.', 'clusterpress' );
		}
		echo '<div class="no-cluster-results">' . $message . '</div>';
	}

	/**
	 * Override parent views so we can use the filter bar display.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function views() {
		global $tab;

		$views = array();
		$tabs = clusterpress()->admin->manage_clusters_tabs;
		$tab = 'all';

		if ( ! empty( $_GET['tab'] ) ) {
			$tab = $_GET['tab'];
		}

		foreach ( (array) $tabs as $action => $text ) {
			$class = ( $action === $tab ) ? ' current' : '';
			$href = add_query_arg( array( 'page' => 'clusterpress-main', 'tab' => $action ), self_admin_url( 'admin.php' ) );
			$views['cluster-'.$action] = "<a href='$href' class='$class'>$text</a>";
		}

		?>
		<div class="wp-filter">
			<ul class="filter-links">
				<?php
				if ( ! empty( $views ) ) {
					foreach ( $views as $class => $view ) {
						$views[ $class ] = "\t<li class='$class'>$view";
					}
					echo implode( " </li>\n", $views ) . "</li>\n";
				}
				?>
			</ul>
		</div>
		<?php
	}

	/**
	 * Display Cluster rows.
	 *
	 * @since  1.0.0
	 *
	 * @return string HTML Output.
	 */
	public function display_rows() {
		global $tab;

		$plugins_allowedtags = array(
			'a' => array( 'href' => array(),'title' => array(), 'target' => array() ),
			'abbr' => array( 'title' => array() ),'acronym' => array( 'title' => array() ),
			'code' => array(), 'pre' => array(), 'em' => array(),'strong' => array(),
			'ul' => array(), 'ol' => array(), 'li' => array(), 'p' => array(), 'br' => array()
		);

		foreach ( (array) $this->items as $key => $plugin ) {
			if ( is_object( $plugin ) ) {
				$plugin = (array) $plugin;
			}

			$title = wp_kses( $plugin['name'], $plugins_allowedtags );

			// Remove any HTML from the description.
			$description = strip_tags( $plugin['short_description'] );
			$version = wp_kses( $plugin['version'], $plugins_allowedtags );

			$name = strip_tags( $title . ' ' . $version );

			$author = wp_kses( $plugin['author'], $plugins_allowedtags );
			if ( ! empty( $author ) ) {
				$author = ' <cite>' . sprintf( __( 'De %s', 'clusterpress' ), $author ) . '</cite>';
			}

			$action_links = array();

			if ( current_user_can( 'install_plugins' ) || current_user_can( 'update_plugins' ) ) {
				$status = install_plugin_install_status( $plugin );

				switch ( $status['status'] ) {
					case 'install':
						if ( $status['url'] ) {
							/* translators: 1: Plugin name and version. */
							$action_links[] = '<a class="install-now button" data-slug="' . esc_attr( $plugin['slug'] ) . '" href="#" aria-label="' . esc_attr( sprintf( __( 'Installer %s maintenant', 'clusterpress' ), $name ) ) . '" data-name="' . esc_attr( $name ) . '">' . __( 'Installer Maintenant', 'clusterpress' ) . '</a>';
						}
						break;

					case 'latest_installed':
					case 'newer_installed':
						if ( is_plugin_active( $status['file'] ) ) {
							if ( isset( $plugin['_built_in'] ) || isset( $plugin['cluster_id'] ) ) {

								if ( isset( $plugin['_built_in'] ) && true === $plugin['_built_in'] ) {
									$action_links[] = '<span class="attention">' . _x( 'Requis', 'cluster', 'clusterpress' ) . '</span>';
								} else {
									$deactivate_args = array(
										'page'    => 'clusterpress-main',
									);

									if ( ! isset( $plugin['_built_in'] ) ) {
										$deactivate_args['plugin']  = $status['file'];
										$deactivate_args['cluster'] = $plugin['cluster_id'];
										$deactivate_args['action']  = 'disable';
									} else {
										$deactivate_args['disable'] = $plugin['_built_in'];
									}

									$deactivate_cluster_url = add_query_arg( $deactivate_args, self_admin_url( 'admin.php' ) );

									if ( ! empty( $deactivate_args['plugin'] ) ) {
										$deactivate_cluster_url = wp_nonce_url( $deactivate_cluster_url, 'cp_disable_plugin_' . $deactivate_args['plugin'] );
									}

									$action_links[] = sprintf(
										'<a href="%1$s" class="button deactivate-now button-secondary" aria-label="%2$s">%3$s</a>',
										esc_url( $deactivate_cluster_url ),
										esc_attr( sprintf( _x( 'Désactiver %s', 'plugin', 'clusterpress' ), $plugin['name'] ) ),
										__( 'Désactiver', 'clusterpress' )
									);
								}
							} else {
								$action_links[] = '<button type="button" class="button button-disabled" disabled="disabled">' . _x( 'Activé', 'plugin', 'clusterpress' ) . '</button>';
							}

						} elseif ( current_user_can( 'activate_plugins' ) ) {
							$cluster_id = '';
							if ( isset( $plugin['_built_in'] ) ) {
								$cluster_id = $plugin['_built_in'];
							} elseif ( isset( $plugin['cluster_id'] ) ) {
								$cluster_id = $plugin['cluster_id'];
							}

							if ( $cluster_id ) {
								$cluster_activate_args = array(
									'page' => 'clusterpress-main',
								);

								if ( isset( $plugin['wp_plugin_id'] ) && empty( $plugin['wp_plugin_is_active'] ) ) {
									$cluster_activate_args = array_merge( $cluster_activate_args, array(
										'plugin'  => $plugin['wp_plugin_id'],
										'cluster' => $cluster_id,
										'action'  => 'enable',
									) );
								} else {
									$cluster_activate_args['enable'] = $cluster_id;
								}

								$activate_cluster_url = add_query_arg( $cluster_activate_args, self_admin_url( 'admin.php' ) );

								if ( ! empty( $cluster_activate_args['plugin'] ) ) {
									$activate_cluster_url = wp_nonce_url( $activate_cluster_url, 'cp_enable_plugin_' . $cluster_activate_args['plugin'] );
								}

								$action_links[] = sprintf(
									'<a href="%1$s" class="button activate-now button-primary" aria-label="%2$s">%3$s</a>',
									esc_url( $activate_cluster_url ),
									esc_attr( sprintf( _x( 'Activer %s', 'plugin', 'clusterpress' ), $plugin['name'] ) ),
									__( 'Activer', 'clusterpress' )
								);
							}
						} else {
							$action_links[] = '<button type="button" class="button button-disabled" disabled="disabled">' . _x( 'Installé', 'plugin', 'clusterpress' ) . '</button>';
						}
						break;
				}
			}

			$cp = clusterpress();
			if ( isset( $cp->admin->settings_tabs[ $key ] ) ) {
				$config_link = add_query_arg( array(
					'page' => 'clusterpress',
					'tab' => $key
				), self_admin_url( $cp->admin->settings_page ) );

				$action_links[] = '<a href="' . esc_url( $config_link ) . '" aria-label="' . esc_attr( sprintf( __( 'Configurer %s', 'clusterpress' ), $name ) ) . '" data-title="' . esc_attr( $name ) . '">' . esc_html__( 'Configurer', 'clusterpress' ) . '</a>';
			}

			if ( is_object( $plugin['icons'] ) ) {
				$plugin['icons'] = (array) $plugin['icons'];
			}

			if ( ! empty( $plugin['icons']['svg'] ) ) {
				$plugin_icon_url = $plugin['icons']['svg'];
			} else {
				$plugin_icon_url = cp_get_includes_url()  . 'admin/img/grape-w.svg';
			}
		?>
		<div class="plugin-card plugin-card-<?php echo sanitize_html_class( $plugin['slug'] ); ?>">
			<div class="plugin-card-top">
				<div class="name column-name">
					<h3<?php echo ( ! empty( $plugin['wp_plugin_slug'] ) ? ' id="' . $plugin['wp_plugin_slug'] . '"' : '' ); ?>>
						<?php echo $title; ?>
						<img src="<?php echo esc_attr( $plugin_icon_url ) ?>" class="plugin-icon" alt="">
					</h3>
				</div>
				<div class="action-links">
					<?php
						if ( $action_links ) {
							echo '<ul class="plugin-action-buttons"><li>' . implode( '</li><li>', $action_links ) . '</li></ul>';
						}
					?>
				</div>
				<div class="desc column-description">
					<p><?php echo $description; ?></p>
					<p class="authors"><?php echo $author; ?></p>
				</div>
			</div>
		</div>
		<?php
		}
	}
}
