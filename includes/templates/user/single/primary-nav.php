<?php
/**
 * ClusterPress Single User Primary Nav template.
 *
 * @package ClusterPress\templates\user\single
 * @subpackage primary-nav
 *
 * @since 1.0.0
 */

if ( cp_user_has_primary_nav() ) : ?>

	<ul data-cp-nav="primary">

		<?php while ( cp_user_nav_items() ) : cp_user_nav_item(); ?>

			<li id="<?php cp_user_nav_item_id(); ?>" class="<?php cp_user_nav_classes(); ?>">
				<a href="<?php cp_user_nav_link(); ?>" title="<?php cp_user_nav_link_title(); ?>">
					<?php cp_user_nav_title(); ?>
				</a>
			</li>

		<?php endwhile; ?>

	</ul>

<?php endif ;
