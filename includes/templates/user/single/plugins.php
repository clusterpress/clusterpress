<?php
/**
 * ClusterPress Single User Plugins template.
 *
 * @package ClusterPress\templates\user\single
 * @subpackage plugins
 *
 * @since 1.0.0
 */
cp_cluster_plugins_template(); ?>
