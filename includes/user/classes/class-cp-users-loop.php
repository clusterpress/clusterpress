<?php
/**
 * ClusterPress Users Loop.
 *
 * @package ClusterPress\user\classes
 * @subpackage users-loop
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Users loop Class.
 *
 * @since 1.0.0
 */
class CP_Users_Loop extends CP_Cluster_Loop {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args the loop args {
	 *    An array of arguments.
	 *    @type int   $offset  The SQL Offset.
	 *    @type int   $number  The number of results to return.
	 *    @type array $include The list of user ids to include.
	 * }
	 */
	public function __construct( $args = array() ) {
		$r = wp_parse_args( $args, array(
			'offset'  => 0,
			'number'  => 10,
		) );

		$paged = get_query_var( 'paged' );

		if ( ! $paged ) {
			if ( ! empty( $_GET['paged'] ) ) {
				$paged = absint( $_GET['paged'] );

			// Default to first page
			} else {
				$paged = 1;
			}
		}

		if ( ! $r['offset'] && ! empty( $r['number'] ) ) {
			$r['offset'] = (int) ( ( $paged - 1 ) * (int) $r['number'] );
		}

		// Do not run a query if included users contains a 0 value
		if ( isset( $r['include'] ) && 0 === $r['include'] ) {
			$u = array();
			$t = 0;

		} else {
			$users = cp_get_users( $r );

			$u = $users['users'];
			$t = $users['total'];
		}

		// Defaults to no pretty links.
		$paginate_args = array( 'base' => add_query_arg( 'paged', '%#%' ) );

		// Pretty links
		if ( clusterpress()->permalink_structure ) {
			$paginate_args['base']   = trailingslashit( cp_get_archive_link( 'user' ) ) . '%_%';
			$paginate_args['format'] = cp_get_paged_slug() . '/%#%/';
		}

		parent::start( array(
			'plugin_prefix'    => 'cp',
			'item_name'        => 'user',
			'item_name_plural' => 'users',
			'items'            => $u,
			'total_item_count' => $t,
			'page'             => $paged,
			'per_page'         => $r['number'],

		/**
		 * Filter here to edit paginate args according to the context.
		 *
		 * @since 1.0.0
		 *
		 * @param array $paginate_args The arguments of the pagination.
		 */
		), apply_filters( 'cp_users_loop_paginate_args', $paginate_args ) );
	}
}
