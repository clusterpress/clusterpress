<?php
/**
 * ClusterPress Single User Comments template.
 *
 * @package ClusterPress\templates\user\single
 * @subpackage comments
 *
 * @since 1.0.0
 */
?>

<h2><?php cp_user_comments_the_loop_title(); ?></h2>

<?php cp_get_template_part( 'assets/filters' ) ; ?>

<div class="view-user-comments">

	<?php cp_get_template_part( 'site/loops/comments' ) ; ?>

</div>
