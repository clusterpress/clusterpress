/* global clusterPress */

( function( $ ) {

	// Bail if not set
	if ( typeof clusterPress === 'undefined' ) {
		return;
	}

	$( '.plugin-action-buttons' ).on( 'click', 'a.install-now', function( event ) {
		var link = $( event.currentTarget ), slug = link.data( 'slug' ), originalText,
		    card = link.closest( '.plugin-card' );

		event.preventDefault();

		if ( link.hasClass( 'updating-message' ) ) {
			return;
		}

		if ( link.html() !== clusterPress.installing ) {
			originalText = link.html();
		}

		card.removeClass( 'plugin-card-update-failed' ).find( '.is-dismissible' ).remove();

		link.addClass( 'updating-message' ).text( clusterPress.installing );

		$.post( clusterPress.ajaxUrl, {
			'action'   : 'cp_install_cluster',
			'_wpnonce' : clusterPress.nonce,
			'slug'     : slug
		}, function( response ) {
			link.removeClass( 'updating-message' ).text( originalText );

			if ( true === response.success ) {
				// Redirect to the Clusters Manage Screen.
				document.location.href = clusterPress.redirect + '&installed=1#' + slug;
			} else {
				card.addClass( 'plugin-card-update-failed' ).append(
					'<div class="notice notice-error notice-alt is-dismissible"><p>' + response.data.errorMessage + '</p></div>'
				);
			}
		}, 'json' );
	} );

} )( jQuery );
