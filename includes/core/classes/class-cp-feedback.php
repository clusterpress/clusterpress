<?php
/**
 * Extends the WP_Error Class to manage user feedbacks.
 *
 * @package ClusterPress\core\classes
 * @subpackage feedback
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * The Feedback class.
 *
 * @since 1.0.0
 */
class CP_Feedback extends WP_Error {

	/**
	 * Feedback code
	 *
	 * @var string
	 */
	private $active_code = '';

	/**
	 * Overrides WP_Error::get_error_code() so that it returns an empty value.
	 *
	 * @since 1.0.0
	 *
	 * @return string an empty string.
	 */
	public function get_error_code() {
		return '';
	}

	/**
	 * Get all or a specific error message
	 *
	 * @since 1.0.0
	 *
	 * @param string $code The error code.
	 * @return array The error messages
	 */
	public function get_error_messages( $code = '' ) {
		if ( empty( $code ) ) {
			return array();
		}

		return parent::get_error_messages( $code );
	}

	/**
	 * Add a feedback entry
	 *
	 * @since 1.0.0
	 *
	 * @param string $code    The feedback code.
	 * @param string $message The feedback message to display.
	 * @param string $type    Whether it's a successful or failed message.
	 * @return array The error messages
	 */
	public function add_feedback( $code = '', $message = '', $type = '' ) {
		$this->add( $code, $message, $type );
		$this->active_code = $code;
	}

	/**
	 * Set as current feedback the given feedback code
	 *
	 * @since 1.0.0
	 *
	 * @param string $code    The feedback code.
	 * @param string $type    Whether it's a successful or failed message.
	 */
	public function set_feedback( $code = '', $type = '' ) {
		$this->add_data( $type, $code );
		$this->active_code = $code;
	}

	/**
	 * Get a feedback entry according to the provided code.
	 *
	 * @since 1.0.0
	 *
	 * @param string $code    The feedback code.
	 * @return array The feedback data for output.
	 */
	public function get_feedback( $code = '' ) {
		if ( empty( $code ) ) {
			$code = $this->active_code;
		}

		$message = $this->get_error_message( $this->active_code );

		if ( ! $message ) {
			return false;
		}

		$type = $this->get_error_data( $code );

		if ( ! $type ) {
			$type = 'error';
		}

		preg_match( '/\[(.*?)\]/', $code, $matches );

		if ( ! isset( $matches[1] ) ) {
			return false;
		}

		$key     = $matches[1];
		$cluster = str_replace( $matches[0], '', $code );

		/**
		 * Filter here to edit the feedback message.
		 *
		 * @since 1.0.0
		 *
		 * @param array $value   The Feedback data.
		 * @param string $key    The Feedback key.
		 * @param string $cluser The Cluster ID.
		 */
		return (array) apply_filters( 'cp_feedback_get_current', array(
			'type'    => $type,
			'message' => $message,
			'css_id'  => $type . '-' . $cluster . '-' . $key,
		), $key, $cluster );
	}
}
