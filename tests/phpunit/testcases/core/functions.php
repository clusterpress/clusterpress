<?php
/**
 * @group nav_menu
 */
class CP_UnitTests_Core_Functions extends CP_UnitTestCase {

	public function test_cp_get_wp_nav_items_not_loggedin() {
		$urls = wp_filter_object_list( cp_get_wp_nav_items(), array( 'object' => 'site' ), 'NOT', 'url' );

		$this->assertTrue( 1 === count( $urls ) );
	}

	public function test_cp_get_wp_nav_items_loggedin() {
		$reset_current_user = get_current_user_id();

		$u = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		wp_set_current_user( $u );

		$urls = wp_filter_object_list( cp_get_wp_nav_items(), array( 'object' => 'site' ), 'NOT', 'url' );

		$this->assertTrue( 2 === count( $urls ) );
		$is_multisite = is_multisite();

		$expected = array(
			'/' . cp_get_root_slug() . '/' . cp_users_get_slug() . '/',
			'/' . cp_get_root_slug() . '/' . cp_user_get_slug() . '/imath/',
		);

		if ( ! $is_multisite ) {
			$expected = array(
				cp_get_root_rewrite_id() . '=' . cp_users_get_slug(),
				cp_user_get_rewrite_id() . '=imath',
			);
		}

		$tested = array();

		foreach( $urls as $url ) {
			$check = parse_url( $url );

			if ( ! $is_multisite ) {
				$tested[] = $check['query'];
			} else {
				$tested[] = $check['path'];
			}
		}

		$this->assertSame( $expected, $tested );

		wp_set_current_user( $reset_current_user );
	}

	public function test_cp_validate_nav_menu_items_not_loggedin() {
		$items = cp_get_wp_nav_items();

		foreach ( $items as $k => $i ) {
			$items[$k]->type = 'clusterpress';
		}

		$tested   = count( cp_validate_nav_menu_items( $items ) );
		$expected = 1;

		if ( is_multisite() ) {
			$expected = 2;
		}

		$this->assertEquals( $expected, $tested );

		if ( is_multisite() ) {
			$active_clusters                = clusterpress()->active_clusters;
			clusterpress()->active_clusters = array_diff( $active_clusters, array( 'site' ) );

			$tested = count( cp_validate_nav_menu_items( $items ) );
			$expected = 1;

			$this->assertEquals( $expected, $tested );

			clusterpress()->active_clusters = $active_clusters;
		}
	}

	public function test_cp_validate_nav_menu_items_loggedin() {
		$reset_current_user = get_current_user_id();

		$u = $this->factory->user->create( array(
			'user_nicename' => 'imath',
		) );

		wp_set_current_user( $u );

		$items = cp_get_wp_nav_items();

		foreach ( $items as $k => $i ) {
			$items[$k]->type = 'clusterpress';
		}

		$tested   = count( cp_validate_nav_menu_items( $items ) );
		$expected = 2;

		if ( is_multisite() ) {
			$expected = 3;
		}

		$this->assertEquals( $expected, $tested );

		wp_set_current_user( $reset_current_user );
	}
}
